**_Constraints can be powerful._**
T       O       T               L       E       A       R       N    

[Dark Odette](shortcuts://x-callback-url/run-shortcut?name=Dark%20Odette) - [Light](shortcuts://x-callback-url/run-shortcut?name=Light%20Odette) - [dark](shortcuts://x-callback-url/run-shortcut?name=dark) - [MacWish](shortcuts://x-callback-url/run-shortcut?name=MacWish) - [LinkJar](shortcuts://x-callback-url/run-shortcut?name=LinkJar) - [TBLink](shortcuts://x-callback-url/run-shortcut?name=tbmac) - [Create Shortcut](shortcuts://create-shortcut) - [Swing Updater](shortcuts://run-shortcut?name=Swing%20Updater) - [Generate 20](shortcuts://x-callback-url/run-shortcut?name=Gen) [-](tg://resolve?domain=raindropiobot) [tgrainbot](tg://resolve?domain=raindropiobot) - **[Apple Frames](shortcuts://run-shortcut?name=Apple%20Frames)** - [Random Scriptable API](scriptable:///run/Random%20Scriptable%20API) - [ScriptBitch](scriptable:///run/ScriptDude) - [Data Jar URL List](shortcuts://run-shortcut?name=Data%20Jar%20URL%20List) - [a](shortcuts://run-shortcut?name=a) - [iOS Soundboard Encoder](shortcuts://run-shortcut?name=iOS%20Soundboard%20Encoder) - [Scriptable Refresh](shortcuts://run-shortcut?name=Scriptable%20Refresh) - [Jorts Device Report](shortcuts://run-shortcut?name=Jorts%20Device%20Report) - [Update Scrubs](shortcuts://run-shortcut?name=Update%20Scrubs) - [a](shortcuts://run-shortcut?name=a) - [Speedy Home Screen](shortcuts://run-shortcut?name=Speedy%20Home%20Screen) - _[DraftsKeysUpdate](shortcuts://run-shortcut?name=DraftsKeysUpdate)_ - [App Store Marketeting Tools](shortcuts://run-shortcut?name=App%20Store%20Marketing%20Tools) - [macspeeds](shortcuts://run-shortcut?name=macspeeds) - [RoutineHub Report](shortcuts://run-shortcut?name=RoutineHub%20Report) - [jobot](shortcuts://run-shortcut?name=jobot) - [Safari Tabs Jar](shortcuts://run-shortcut?name=Safari%20Tabs%20Jar) - [Update PFWTL](shortcuts://run-shortcut?name=Update%20PFWTL) - [gurgle](shortcuts://run-shortcut?name=gurgle) - [ttm](shortcuts://run-shortcut?name=ttm) - [HexScreen](shortcuts://run-shortcut?name=HexScreen) - [ts](shortcuts://run-shortcut?name=ts) - [DTwitter](shortcuts://run-shortcut?name=DTwitter) - **[Speedy Frames](shortcuts://run-shortcut?name=Speedy%20Frames)** - [README](shortcuts://run-shortcut?name=Drafts%20README%20Update) - [ttr](shortcuts://run-shortcut?name=TrippleTap%20Reminder) - [Logger Push](shortcuts://run-shortcut?name=Logger%20Push) - **[Tot Git](shortcuts://run-shortcut?name=Tot%20Git%20iOS-NoDev)** - [DOWNLOAD](shortcuts://run-shortcut?name=DOWNLOAD) - [SW-DLT](shortcuts://run-shortcut?name=SW-DLT) - [rss](shortcuts://run-shortcut?name=rss) - [Artist List Push](shortcuts://run-shortcut?name=Update%20MusicHarbor%20Artists%20List) - [3Remind](shortcuts://run-shortcut?name=TrippleTap%20Reminder) - [ALL](shortcuts://run-shortcut?name=Open%20All%20URLS%20in%20Clipboard) - [Vocabulary Index](shortcuts://run-shortcut?name=Vocabulary%20Index)

[symbol](shortcuts://run-shortcut?name=symbol)

[RoutinePub](shortcuts://run-shortcut?name=RoutinePub)
[Shortcut Source](shortcuts://run-shortcut?name=Shortcut%20Source%20Tool)
[Action Directory](shortcuts://run-shortcut?name=Action%20Directory)
[iOSSystemSounds](https://extratone.github.io/iOSSystemSounds)
[RoutineHub - Create Shortcut](https://routinehub.co/shortcut/create)

_New Issues_
- [i](https://github.com/extratone/i/issues/new/choose)
- [bilge](https://github.com/extratone/bilge/issues/new/choose)
- [drafts](https://github.com/extratone/drafts/issues/new/choose)

**Drafts Instrument Panel**
_[Master Index](https://tilde.town/~extratone/drafts/big/)_

**[Automation April](drafts://open?uuid=114E449F-7518-4158-87EB-C817F633FBF2)**

_Drafts_
- [End User Episode 9](drafts://open?uuid=69EA3CC6-7081-47B4-B4AB-C16E5C7E8417)
- [Fresh Eyes on macOS](drafts://open?uuid=944F772C-CC9D-47F2-BD98-F4E435E40227)
- [Hold Your Clipboard Mangers Close](drafts://open?uuid=81CC3D50-BC7A-471F-B247-DBE0ACC54C6B)
- [David Blue’s Twitter History](drafts://open?uuid=F490F4FE-5635-4513-9320-B5DB7E0FFE31)
- [How to Fuck Text](drafts://open?uuid=A350D578-1CFA-4B5A-8655-74F6B3AD72AE)
- [Please Fuck With This License](drafts://open?uuid=FAFFA333-34A5-4412-B46C-3D216C09FD8E)
- [Using Drafts with NeoCities](drafts://open?uuid=7247282E-340B-4890-A2F7-0481AF31321E)
- [Underdocumented Maneuvers in iOS](drafts://open?uuid=6271A84E-D0F9-4881-A20D-B22E88AA9E3B)
- [How I Manage Drafts](drafts://open?uuid=0BF34703-065D-4A2F-9F89-381A2348405E)

_Notes_
- [Notes-Fresh Eyes on macOS](drafts://open?uuid=6C76EDBD-025D-40BA-9381-9AE95AD70F36)
- [Notes-Hold Your Clipboard Managers Close](drafts://open?uuid=65AC8211-F3F9-419D-A7B3-EBF82E4944F1)
- [Notes-Personal Twitter History](drafts://open?uuid=37B865E1-4372-4AF7-BB77-3979836130D4)
- [Notes-TextFuck](drafts://open?uuid=F0E9A527-77DE-45C6-9B24-F101565AC0BE)
- **[Drag Safari Tab Group Links Demo](drafts://open?uuid=4FBF3BCC-2F65-4C8B-9486-839E118B4B3A)**
- [Notes-Using Drafts with NeoCities](drafts://open?uuid=70C7C7DF-5083-411A-A397-A14479CA90BF)
- [Notes-Drafts vs. Obsidian](drafts://open?uuid=C300D6BD-07A2-44E2-9231-54ADA771F952)
- [Remark.as Integration](drafts://open?uuid=B0C95CA5-E5DE-4478-9C09-1E30499438CE)
- [Notes-How I Manage Drafts](drafts://open?uuid=939FF8C4-CB56-4A5F-99E9-D88E7162BE4E)

_Social/Family/Support_
- [Suggestions/Requests for a WebDAV (NeoCities, in my case) Action Group](https://forums.getdrafts.com/t/suggestions-requests-for-a-webdav-neocities-in-my-case-action-group/11994)
- [RainDrop Import](https://app.raindrop.io/settings/import)
- [Automating Write.as Posts on macOS with Siri Shortcuts calling the Writeas CLI - Development / Platform - Discuss Write.as](https://discuss.write.as/t/automating-write-as-posts-on-macos-with-siri-shortcuts-calling-the-writeas-cli/4484)
- [David Blue on Tilde.Town](drafts://open?uuid=53D80A72-BE50-48E3-A25C-0799764CE4F2)
- [A set of Drafts Actions for interacting with your NeoCities site via WebDAV. : r/neocities](https://www.reddit.com/r/neocities/comments/si7ydf/a_set_of_drafts_actions_for_interacting_with_your/)
- [Underdocumented iOS Manuevers Thread](https://www.reddit.com/r/ios/comments/l5p7ly/underdocumented_ios_functions_stuff_you_wish/)
- [Recommended Apps](drafts://open?uuid=C11B9C28-5584-414B-8C22-D0ACFCA339D0)
- [Tilde Guide Tweet](https://twitter.com/NeoYokel/status/1503187276444098560)
- [Automation April](drafts://open?uuid=114E449F-7518-4158-87EB-C817F633FBF2)
- [Introducing Automation April  A Month-Long Community Event About Automation, Featuring Shortcuts, Interviews, Discord Workshops, and a Shortcut Contest](drafts://open?uuid=ECDF27E4-A224-4A9C-BDCD-3B3F9CA8B927)
- [Mattcito Help-MacStories Discord](drafts://open?uuid=424FFFE3-0335-4398-8588-A9149CD0A208)
- [Drafts Cult Telegram Group](drafts://open?uuid=388B2827-47FA-4F26-8467-F4DED3805E4D)
- [I have too many drafts to handle!-Drafts Forums](drafts://open?uuid=B1E83A7F-9C31-4881-B608-FA1E12D3B17D)
- [Out & About Speedtest Thread](https://twitter.com/neoyokel/status/1501949196957589506)

_Documentation_
- **[Drafts Keyboard Shortcuts](drafts://open?uuid=F55AB610-D9F9-49D1-9C20-1CF6063DF03E)**
- [Taio Image Uploader and Snap.as](drafts://open?uuid=DD9A7373-F763-4B84-853C-6964F4F3B39D)
- [Distributed Index](drafts://open?uuid=2BE147C7-D5E2-41E0-91B1-F65EBEFAA81A)
- [Siri Shortcuts for Telegram](drafts://open?uuid=3260C6BA-1CA8-4ADB-9888-98601FADD82F)
- **[David Blue’s RoutineHub Library](drafts://open?uuid=2145B3D6-2DE4-43E9-A305-3C879D7027DD)**
- [Scrubs Shortcuts Library](drafts://open?uuid=68F049EC-59AF-4A49-885F-698BB9653400)
- [Highlights](drafts://open?uuid=98CAEC13-3E7B-425C-B389-E5E0BC06E935)
- [Raindrop Telegram Bot](drafts://open?uuid=CB2985C0-C484-48F0-A170-2EBEF1E94972)
- [NeoCities/WebDAV Action Group](drafts://open?uuid=3BA64907-441A-4F34-9F4D-DB39E6EFF898)
- [Drafts Actions Index](https://davidblue.wtf/drafts/actionsindex)
- [ForkLift 3 - User Manual - most advanced file manager and FTP client for macOS](https://binarynights.com/manual)
- [Shortcuts x-callback URLs](drafts://open?uuid=B47A3F69-12B7-4EA7-BBFA-ABB0FFBABCB8)
- [David Blue’s Drafts Action Directory Contributions](drafts://open?uuid=C70FF15A-5B48-407C-9587-0DC4E9536102)
- [TextExpander Snippet Groups](drafts://open?uuid=00ED31D1-18B9-4A61-ADF9-3128DD0C2688)
- [WordCloudjs API](https://github.com/timdream/wordcloud2.js/blob/gh-pages/API.md)
- [internetarchive CLI Docs](bear://x-callback-url/open-note?id=2A87D668-F2F8-4634-93FC-194EA2393D03-35818-0000034E74468AE3)
- [On My Mind Action Group by FlohGro](drafts://open?uuid=C90B70B3-A594-4736-86B2-E4F1924D3096)
- [WTF Redirection](drafts://open?uuid=218F8FAA-C0B9-4B4F-B896-3089E005E86E)

[New Draft with Clipboard](drafts://x-callback-url/create?text=%7C%7Cclipboard%7C%7C)

[Keyboard Shortcuts Export](drafts://runAction?test=&action=Keyboard%20Shortcuts%20Export)

_!!!_
- [Neil Harrison Rave Library: 1990s Rave Mixes : Free Audio : Free Download, Borrow and Streaming : Internet Archive](https://archive.org/details/neilharrisonravelibrary)

_Workspaces_
[Hole](drafts://x-callback-url/workspace?name=Hole) - [Written](drafts://x-callback-url/workspace?name=Written) - [Documentation](drafts://x-callback-url/workspace?name=Documentation) - [Local](drafts://x-callback-url/workspace?name=Local) - [NeoCities](drafts://x-callback-url/workspace?name=NeoCities) - [Dev](drafts://x-callback-url/workspace?name=Dev) - [Keys](drafts://x-callback-url/workspace?name=Keys) - [Correspondence](drafts://x-callback-url/workspace?name=Correspondence) - [Social](drafts://x-callback-url/workspace?name=Social) - [Small](drafts://x-callback-url/workspace?name=Small)
[Publish](drafts://x-callback-url/workspace?name=Publish) - [Templates](drafts://x-callback-url/workspace?name=Templates) - [Vocabulary](drafts://x-callback-url/workspace?name=Vocabulary) - [Dots](drafts://x-callback-url/workspace?name=Dots) - [Feebles](drafts://x-callback-url/workspace?name=Feebles)
**Tot Keys**
- **⌘/** = Show/Hide Status Bar
- **⇧⌘T** = Switch to Plain Text View
- **⌘,** = Preferences
- **⌘⌥,** = Theme Preferences
- **⌘⌥⇦⇨** = Tot Switch
- **⌘0** = Empty Tot
- **⇧⌘S** = Save File
- **⌘S** = Save Version
- **⌥⌘S** = Save Backup
- **⌥⌘R** = Restore Backup

**Todo**
- [Write.as Siri Shortcuts](drafts://open?uuid=C616958C-BFDE-4B6C-A7E9-05CBAF394E08)
- [Generate Shortcuts x-callback Links (macOS)](drafts://open?uuid=400D3AE6-CBA9-4272-B681-92B9131ED7B3)
- WhatCMS API tools
- AppWishIDs
- Batch rename image files by sampling from an Extract Text from Image Action, intelligently truncated.
- Post to Write.as Blog (iOS) - with a-Shell?
- Taio & Drafts Integration with [URL Schemes](https://docs.taio.app/#/integration/url-schemes)
- Drafts Themes inspired by [Mineshaft Gold](https://t.me/addtheme/mineshaftgold2022) and [Japan Serenity](https://t.me/addtheme/japanserenity2022)
- Big Complex [iOS System Sounds](https://github.com/extratone/iOSSystemSounds) shortcut
- SSH with Working Copy
- Learning to use the Snap.as CLI/Snap.as Siri Shortcuts
- Shortcuts Horn Buttons
- Bear links to Tot?
- Figure out Zalgo Shortcuts
- Figure out how GitHub Wikis actually work and organize [The Psalms’ Wiki](https://github.com/extratone/bilge/wiki)
- WriteFreely community Telegram Group?
- Learn [Paste Stacks](https://pasteapp.io/help/paste-stack-51)
- Update [NeoCities Homepage Text](drafts://open?uuid=D1E2EEA4-95BC-4837-BE96-387BC11E0037)
- LookUp Collection ⇨ Spoken Audio File
- [Submit to](https://www.earratmag.com/p/submissions.html) _[Ear Rat](https://www.earratmag.com/p/submissions.html)_[’s 90s Issue by March 31st](https://www.earratmag.com/p/submissions.html)
- A Safari Extension for iOS that cleans links
- Bear Siri Shortcuts
- Bear Keyboard Shortcuts
- Bear URL Schemes
- More extensive Panda testing?
- Services/Shortcuts in macOS
- New Draft with current Safari tab links function on iOS via Shortcuts
- Siri Shortcuts based on the [Tumblr API](https://www.tumblr.com/docs/en/api/v2)
- **Doubling the WebDAV action steps fixes everything???**
⌦ ⌦ ⌦ ⌦ 

**Reading**
- [Toolbox Pro Review: A Must-Have Companion Utility for Shortcuts Power Users](https://www.macstories.net/reviews/toolbox-pro-review-a-must-have-companion-utility-for-shortcuts-power-users/)
- [Drafts 15 Review: Multiwindow, Shortcuts, and More](https://www.macstories.net/reviews/drafts-15-review-multiwindow-shortcuts-and-more/)
- “[Automating iOS: A Comprehensive Guide to URL Schemes and Drafts Actions](drafts://open?uuid=A944CF95-DE24-47AA-BC50-CB03E1A20396)”
- [Best way to learn regular expressions : shortcuts](https://www.reddit.com/r/shortcuts/comments/tv4o6b/tip_best_way_to_learn_regular_expressions/)


**Tools**
- [ColorHound](http://colorhound.net)
- [HTML HOUSE](https://html.house)

[Silicon Valley Buzz: Apple Paid More Than $200 Million For Siri To Get Into Mobile Search | TechCrunch](https://techcrunch.com/2010/04/28/apple-siri-200-million/)

<https://itunes.apple.com/us/app/workflow-powerful-automation-made-simple/id915249334>

<https://web.archive.org/web/*/https://itunes.apple.com/us/app/workflow-powerful-automation-made-simple/id915249334>


_No highs? 
No lows?
It must be Bose!_

[TWIT JUT](https://botsin.space/@commandandcontrol/107686912492769882)

[UNDER A FULL PROCUREMENT MOON.](https://twitter.com/tomcritchlowBOT/status/1488829725606924291)

**Bear Trial**

_[BearTest](bear://x-callback-url/open-note?id=E2DE96E8-7C98-4403-AD5B-586C2048FFC5-52345-000061152F43AC38)_ - [Bear Test II](bear://x-callback-url/open-note?id=E2DE96E8-7C98-4403-AD5B-586C2048FFC5-52345-000061152F43AC38&title=Testing&header=Test%20Header&exclude_trashed=no&new_window=yes&open_note=yes&pin=yes&edit=yes)

- [On Drafts and Obsidian](bear://x-callback-url/open-note?id=1370F643-9CD2-4B55-8C06-61A596AFA4C0-596-0000000E21830D7A) (Bear)
- [How to Fuck Text](bear://x-callback-url/open-note?id=9F12813D-74E7-498B-8CC4-1B23DB6B33C7-20824-000024D69F569FB9)
- [iPad Diaries: Working with Drag and Drop – Bear and Gladys](bear://x-callback-url/open-note?id=F5F74BDA-3884-4376-9538-0CDDDAE5E1A9-20824-000024AC62301A7C)
- [Why I’m Considering Bear as a Notes App Replacement
-](bear://x-callback-url/open-note?id=ADA46854-09EE-479E-BC4E-ADD5604A62D5-20824-000024A929A0E1A7) 

- [Solar Witch](https://witch.solar)
- Tilde friend reminded me about [Low Tech Magazine](https://solar.lowtechmagazine.com). it’s been years!! how did I lose this bookmark‽‽‽

**Creative Wellness**
- Gran Turismo Sounds
**Siri Shortcuts for Tot**

_RoutineHub_
- [RoutineHub • Tot Jar](https://routinehub.co/shortcut/11456/)
- [RoutineHub • Hot Tot Dot Swap](https://routinehub.co/shortcut/11457/)
- [RoutineHub • Dot Info](https://routinehub.co/shortcut/11459/)
- [RoutineHub • Tot](https://routinehub.co/shortcut/11500/) [⇨](https://routinehub.co/shortcut/11500/) [Apple Notes](https://routinehub.co/shortcut/11500/)
- [RoutineHub • Tot Markdown Backup](https://routinehub.co/shortcut/11458/)
- [RoutineHub • Markdown Link](https://routinehub.co/shortcut/11504/) [⇨](https://routinehub.co/shortcut/11504/) [Tot Dot](https://routinehub.co/shortcut/11504/)
- [RoutineHub • Reminders List](https://routinehub.co/shortcut/11539/) [⇨](https://routinehub.co/shortcut/11539/) [Tot](https://routinehub.co/shortcut/11539/)
- [RoutineHub • Clear Dot](https://routinehub.co/shortcut/11540/)
- [RoutineHub • Dot to New Draft](https://routinehub.co/shortcut/11541)
- [RoutineHub • Dots Summary](https://routinehub.co/shortcut/11542/)

_Drafts_
- [Tot Jar Shortcut](drafts://open?uuid=7A7DCF8B-BAE7-40B4-96E8-311BFC769722)
- [Hot Tot Dot Swap Shortcut](drafts://open?uuid=1E11D78E-134E-4583-A6E1-414855E66312)
- [Tot Markdown Backup](drafts://open?uuid=F145474F-4CD1-4053-BA14-5CFF16857362)
- [Dot Info Shortcut](drafts://open?uuid=5C07CFF1-1F28-4AA0-9704-FF52939536D3)
- [Tot](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0) [⇨](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0) [Apple Notes Shortcut](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0)
- [Markdown Link](drafts://open?uuid=706A5C55-1E97-47A3-AA25-CB1DD29C821D) [⇨](drafts://open?uuid=706A5C55-1E97-47A3-AA25-CB1DD29C821D) [Dot Shortcut](drafts://open?uuid=706A5C55-1E97-47A3-AA25-CB1DD29C821D)
- [Reminders List](drafts://open?uuid=B5C542E5-CDDA-48CE-909A-8A2B028B9368) [⇨](drafts://open?uuid=B5C542E5-CDDA-48CE-909A-8A2B028B9368) [Tot Shortcut](drafts://open?uuid=B5C542E5-CDDA-48CE-909A-8A2B028B9368)
- [Clear Dot Shortcut](drafts://open?uuid=F4F062E4-F265-40A3-B101-03413D7996F8)
- [Dots Summary Shortcut](drafts://open?uuid=7EB80DE5-1074-43E6-B2D3-213A2CD35D5B)
- [Tot Git-iOS](https://www.icloud.com/shortcuts/69aa72b435b94d5f82106e4309303072)
- [Tot Git-macOS](https://www.icloud.com/shortcuts/2597158919384c3eb706230b025abe16)
- [Tot Git Restore-macOS](https://www.icloud.com/shortcuts/0196edc014534e61bcb3f8b372c314d7)
- [Tot Actions Showcase](https://www.icloud.com/shortcuts/06b41c1e86b8485bbfa7f8aafa82b127)
- [Dot to New Draft Shortcut](https://davidblue.wtf/drafts/D20C28B8-6BCA-465F-9F4E-30F4EDA4D0E2.html)
- [Tot](drafts://open?uuid=CA6CA860-67E8-45F8-865E-DF530E4D7F36) [⇦⇨](drafts://open?uuid=CA6CA860-67E8-45F8-865E-DF530E4D7F36) [Drafts Sync Shortcuts](drafts://open?uuid=CA6CA860-67E8-45F8-865E-DF530E4D7F36)
- 

- [The Psalms Tot Theme](drafts://open?uuid=5156ED3A-7C1C-4B3B-9F03-6B977526D5DA)


- [Siri Shortcuts and More for Tot](drafts://open?uuid=9DAFE8FC-3655-4FBD-BA16-7439BAC109FA)

- [Notes-Siri Shortcuts and More for Tot](drafts://open?uuid=E6D127B8-B5CE-45F5-8D6A-0BC5CE12A149)
- 

**Tot Shortcuts Ideas**
- Finish Tot ↓ Drafts
- 50-100 Most Recent Drafts List
[Hot Tot Dot Swap](shortcuts://run-shortcut?name=Hot%20Tot%20Dot%20Swap)



- [Batch Shortcuts Signer (macOS) Shortcut](drafts://open?uuid=5D4D9120-69EC-4449-AB7F-35C0D97BBA7D)
- [David Blue’s Siri Shortcuts Repository
-](drafts://open?uuid=7998F086-4951-4DAB-995F-FE698FACFF78) 

**※ G o o d M o r n i n g ※
**[David Blue](https://davidblue.wtf/)
￼
**I'm David Blue - an automotive journalist on sabbatical and self-described _Software Historian_.**

It's good to be here!

By that I mean, it's especially nice to feel welcome here even though **I am in no way a software developer**.

I use the term _Software Historian_ because I have found myself especially fascinated with the origin stories of the platforms and services we use in the past 3-4 years, especially regarding Word Processors.

My _ultimate_ software development aspiration would be to build my own CMS with Python from scratch, but I'm not just being humble when I tell you that I am _very_ far away from doing that.

Recently, I have been toying with the idea of writing a _Literary History of Streaming Music_, directly inspired by Professor Matthew G. Kirschenbaum's _[Track Changes: A Literary History of Word Processing](https://www.hup.harvard.edu/catalog.php?isbn=9780674417076)_, continuing the work I began in one of my all-time best essays **[examining the Holiness of Bandcamp](https://davidblue.wtf/essays/bandcamp)** as a technology company who can do no wrong.

I'm sure anyone reading this can relate when I lament that _I really should get around to writing an updated bio_, but I've already spent enough time on this one, I think. Let me just _quote myself_ from my blog's about page:

_I’ve already born witness to innovation bridging divisions between people throughout the greatest informational renaissance my species has ever seen... Originally, I wrote about our relationship with **cars** ([2019 Volkswagen Atlas Review](https://dieselgoth.com/2019-volkswagen-atlas-sel-vr6-review)) and now **technology** ([Bandcamp: Streaming's Secret Savior](https://bilge.world/bandcamp-streaming-music)) from a perspective that feels tedious or abstract to some, but is generally entertaining, through-and-through. I’m proud of the work I’ve done so far, which has been wild, absurd, reflective, and hilarious — occasionally all in the same work as I develop my voice._

I'm not just horsing around when I say **feel free to <email me> about literally anything**.

† [The Psalms](https://bilge.world/) (My technology blog. Entirely nonsecular, I promise)
† 

🇪🇺 _[End User](https://anchor.fm/davidblue)_ (A solo "podcast" which I update now and then, talking to my phone - silence truncated - about things I've been exploring.)



⌨ 
⌨ [The iPhone x Bluetooth Keyboard Project](https://uikeycommand.neocities.org/) (Perhaps my very first truly worthwhile cause.)
Ǝ _[Extratone](https://extratone.com/)_ (An online music magazine which I devoted most of my adult life to developing up until 2018.)

⎆ [Social Directory](https://rotund.notion.site/9fdc8e9610b34b8f991ebc148b760055?v=c170b58650c04fbdb7adc551a73d16a7) (An ongoing list of links to my profiles on every social service I can remember signing up for.)
⎆ dghj
⎆ 


[MASTODON](https://mastodon.social/@DavidBlue) **//** [TWITTER](https://twitter.com/NeoYokel) **//** [DISCORD](https://discord.gg/4hdQcVd)
**Site Stuff**
※ [Z](https://davidblue.wtf/zalgo/)[҉̳͕̗͖͕͈͡](<https://davidblue.wtf/zalgo/>) [̶̡͏̟͙A̧̠͈̝̣̺ ̣̳͖̬̺͔̬̜L̻̪͔͚̀ ̨͇̦̕G̸̸͍͕̹̠̳̟͙̬͞ ̛̬̣̱̮͈͚̬ͅO̡̨̦̪̺̯̹͔̙͡ͅ](<https://davidblue.wtf/zalgo/>)' ' ' ※ [ＭＥＧＡ](https://davidblue.wtf/cool/) [ＣＯＯＬ](https://davidblue.wtf/cool/) [ＴＥＸＴ](https://davidblue.wtf/cool/) [UNI1PG](https://davidblue.wtf/tools/unicode.pdf)
⎃⎃⎃⎃⎃⎃⎃⎃ d i n k u s ⎃⎃⎃⎃⎃⎃⎃⎃
	•	Hosted on [Neocities](https://neocities.org/site/davidblue). View website source code on [GitHub](https://github.com/extratone/xyz).

- [On Drafts and Obsidian](drafts://open?uuid=56EED87E-678E-4248-8E94-31650615C69A)
- [Notes-Drafts vs. Obsidian](drafts://open?uuid=C300D6BD-07A2-44E2-9231-54ADA771F952)

- [Notes-Drafts vs. Obsidian](drafts://open?uuid=C300D6BD-07A2-44E2-9231-54ADA771F952)

- [NeoCities Homepage (WTF) 2.0](drafts://open?uuid=C2B1C951-518F-43FA-AEED-020CD5102A7B)

- [Re  screaming](drafts://open?uuid=2DFBD718-8845-49CF-A343-8BA3B639B7B3)

- [Introducing Automation April  A Month-Long Community Event About Automation, Featuring Shortcuts, Interviews, Discord Workshops, and a Shortcut Contest](drafts://open?uuid=ECDF27E4-A224-4A9C-BDCD-3B3F9CA8B927)

- [WTF Drafts Index](drafts://open?uuid=8C4596E3-0397-468C-A579-CF56CB7CFE06)

- [Snap.as Write.as API](drafts://open?uuid=BD1C9DBB-3C89-4FD9-AC9F-4D43BF2C7F08)

- [HTML Redirect](drafts://open?uuid=B3B67F8F-EFA0-4B2A-9BA3-928D1D648B3D)


- [Marco! Shortcut Documentation](drafts://open?uuid=AD8A1DEF-DB4E-491D-B680-23323228D2D5)

<https://ttm.sh/btA.MP4>
<https://ttm.sh/tI5>
<https://ttm.sh/btS.02.png>
<https://ttm.sh/bFm.11.png>


- [gh2md README](drafts://open?uuid=F571E936-9BF5-42F9-B757-2EF52012F90B)
- [Extranet Redirection](drafts://open?uuid=1C547BA9-D29F-4F2E-8B24-3F5872D329B6)
- [Feebles in Night, Second Edition](drafts://open?uuid=7C1047E5-07D2-467E-B488-00DA8EEAD8AD)
- [Why I Didn’t Follow You Back](drafts://open?uuid=052A0D33-79E7-4069-86E7-3C6AAA31DA4A)
- [Keyboard shortcut restrictions](drafts://open?uuid=8B069E6C-3320-43E6-89A0-9EE886C5815E)
- [Query Snap.as Library Shortcut](drafts://open?uuid=A86CAE9C-8F0A-435C-A659-51E2EF92255C)
- [Taio Image Uploader and Snap.as](drafts://open?uuid=DD9A7373-F763-4B84-853C-6964F4F3B39D)
- [Gran Turismo 7 Playlist](drafts://open?uuid=557AD418-F946-495D-93F3-7A7B07E5E56E)
- [Matthew Cassinelli Member Shortcuts](drafts://open?uuid=F253EC60-AF9B-4272-A585-F39E3FEDE49F)
- [Turning Red](drafts://open?uuid=88725F97-A787-4D34-A235-DEEC5369E9C2)
- [Snap.as Embedded Image Display Issue](drafts://open?uuid=171E93E9-3222-4491-844E-EEF6DD0FFA35)
- [I Finally Reached Computing Nirvana. What Was It All For      WIRED](drafts://open?uuid=4427754F-FFB9-4C27-A907-CE88CA6D7004)
- [Drafts Raindrop Collection](drafts://open?uuid=8B5A2F0A-5FCE-4A8F-817B-AB61C7019C86)
- [Current Draft Info Action](drafts://open?uuid=BBFDC458-1049-4FA3-A506-DE6C397B4C63)
- [Twitter Muted Words](drafts://open?uuid=B5A34BE2-DE76-4B3E-B5D5-4D3620519218)
- [Drafts Keyboard Shortcuts (iOS Export)](drafts://open?uuid=880E8C09-09D6-4712-B33F-D0D1630DDC3C)
- [Drafts Keyboard Shortcuts](drafts://open?uuid=F55AB610-D9F9-49D1-9C20-1CF6063DF03E)
- [Gap 1997 (Cassette Rip)](drafts://open?uuid=14774725-923B-4C82-8947-28CA651C838B)
- [Mac Menu Bar Shortcuts](drafts://open?uuid=68489605-5A2E-4250-8B8C-EC651CAEA449)
- [Mac Menu Bar Shortcuts](drafts://open?uuid=68489605-5A2E-4250-8B8C-EC651CAEA449)
- [BBEdit Learning](drafts://open?uuid=816D6041-B902-4E3D-9441-39D77C3748BC)
- [Drafts Twitter List](drafts://open?uuid=355A0209-6184-4FA5-B33D-F70AE50867B1)

- [iOS System Sounds (WTF) Index](drafts://open?uuid=930C8B20-5C2F-4ECC-A311-4DFBF609AFD8)

- [macOS System Sounds Index](drafts://open?uuid=55B87318-A881-491D-8285-6D68456A144A)

- [Titanic Day 2022](drafts://open?uuid=D26EEAE5-A1BB-4C51-8ECB-7CC2391136AC)

- [tmo on Mobile Blogging](drafts://open?uuid=8902BB5A-4CB0-4CE0-BA95-D4A083DEE584)

- [The Monsanto Redbubble Thing](drafts://open?uuid=4E8BDB6A-E703-40FB-9123-C033797CD038)

- [NeoCities API](drafts://open?uuid=16C5D12C-162A-4AAA-AA4F-689DC7057B1E)

- [Spy Kids 3 Spam (iOS) Shortcut Documentation](drafts://open?uuid=91AE0A80-CC72-4387-8545-96030CAD6338)
**qURL Schemes Learning**

_Links_
- [Drafts](https://docs.getdrafts.com/docs/automation/urlschemes)
- [Use x-callback-url with Shortcuts on Mac - Apple Support](https://support.apple.com/guide/shortcuts-mac/use-x-callback-url-apdcd7f20a6f/mac)
- [Automating iOS: A Comprehensive Guide to URL Schemes and Drafts Actions - MacStories](https://www.macstories.net/tutorials/guide-url-scheme-ios-drafts/)
- [Apps | x-callback-url](http://x-callback-url.com/apps/)
- [Use x-callback-url with Shortcuts on iPhone or iPad - Apple Support](https://support.apple.com/guide/shortcuts/use-x-callback-url-apdcd7f20a6f/5.0/ios/15.0)
- [Supported Tweetbot 3 URL Schemes](https://tapbots.net/tweetbot3/support/url-schemes/) - [Draft](drafts://open?uuid=D9CEDA79-9CC1-489F-8684-5329F8C0BB93)
- [Working Copy Keyboard Shortcuts](drafts://open?uuid=2B34F2A2-2299-4C33-ABAB-4D4AE83AF711)
- 

‽‽‽
 
- [Compiling and Exporting Tagged Notes in Drafts 5](drafts://open?uuid=EACE4FDC-30D1-4802-A4DF-01C4DCE36885)
- [ActionGroupReference](drafts://open?uuid=3BA64907-441A-4F34-9F4D-DB39E6EFF898)
- [NeoCities Action Group Reference](drafts://open?uuid=5CA49C6C-0DFC-4A84-A31B-48040FC8BE6E)
- [The Psalms Action Group](drafts://open?uuid=E1AAA4EB-951F-4350-A3E7-C59526FBA1BE)
- [Assorted Unlisted David Blue Configurables](drafts://open?uuid=AC162701-9C53-449E-82AF-162581EB332C)

- [On Configuration Culture](drafts://open?uuid=C06D5FA1-F9BD-45D6-B0E6-F61A648589B8)
- [Remark.as](drafts://open?uuid=1504A9F7-D6B9-4FFD-B073-538A3F3A7C80)

- [Mickey GitHub Repository](drafts://open?uuid=917A11D9-1E52-4CC4-BFDB-419DB685C49F)
- [Pocket Casts Podcasts Export](drafts://open?uuid=2CDEC931-1921-44A9-B5E8-E38402F49CB5)
- [Paste to Jar Shortcut](drafts://open?uuid=65931FF1-96A9-49AF-99C8-FD2CD776C775)
- 
- [Reply to Karenwhite](drafts://open?uuid=2EDCC93C-2346-4F03-82B8-9F0D323D309B)
- [Paste Shortcuts for Mac and iOS](drafts://open?uuid=59314DD1-4DC2-4835-B47C-817D125D2EEE)
- [Notes-Drafts vs. Obsidian](drafts://open?uuid=C300D6BD-07A2-44E2-9231-54ADA771F952)
- [Notes-Shortcuts for Paste](drafts://open?uuid=D459A766-03AD-44C7-BFD9-1B09D49C3A5B)
- [Paste Shortcuts for Mac and iOS](drafts://open?uuid=59314DD1-4DC2-4835-B47C-817D125D2EEE)
- [Notes-Paste Shortcuts for Mac and iOS](drafts://open?uuid=59314DD1-4DC2-4835-B47C-817D125D2EEE)
- [Zalgo Text in a Keyboard Shortcut](drafts://open?uuid=775BA8C5-09A4-47C3-9B88-48D923BDB55D)
- [DRAFTS ZALGOS ACTION](drafts://open?uuid=2A62DD6D-FBAF-4BA4-BC9C-96DC4FDC6A24)
- [On Drafts and Obsidian](drafts://open?uuid=56EED87E-678E-4248-8E94-31650615C69A)
- [LookUp Collection](drafts://open?uuid=2E137CC9-2D0C-4340-B5BC-DBBDE3B8BF01) [⇨](drafts://open?uuid=2E137CC9-2D0C-4340-B5BC-DBBDE3B8BF01) [Clipboard](drafts://open?uuid=2E137CC9-2D0C-4340-B5BC-DBBDE3B8BF01)
- [Unlisted Drafts Directory Links](drafts://open?uuid=0314FCBD-8CC6-4E29-B0A8-F6C69498ED5E)

- [David Blue‘s Drafts Configs](drafts://open?uuid=38A942A2-33C4-4630-9CDE-B005989F4E3A)
- 
- [gh2md README](drafts://open?uuid=F571E936-9BF5-42F9-B757-2EF52012F90B)
- [Extranet Redirection](drafts://open?uuid=1C547BA9-D29F-4F2E-8B24-3F5872D329B6)
- [Feebles in Night, Second Edition](drafts://open?uuid=7C1047E5-07D2-467E-B488-00DA8EEAD8AD)
- [Why I Didn’t Follow You Back](drafts://open?uuid=052A0D33-79E7-4069-86E7-3C6AAA31DA4A)
- [Keyboard shortcut restrictions](drafts://open?uuid=8B069E6C-3320-43E6-89A0-9EE886C5815E)
- [Query Snap.as Library Shortcut](drafts://open?uuid=A86CAE9C-8F0A-435C-A659-51E2EF92255C)
- [Taio Image Uploader and Snap.as](drafts://open?uuid=DD9A7373-F763-4B84-853C-6964F4F3B39D)
- [Gran Turismo 7 Playlist](drafts://open?uuid=557AD418-F946-495D-93F3-7A7B07E5E56E)
- [Matthew Cassinelli Member Shortcuts](drafts://open?uuid=F253EC60-AF9B-4272-A585-F39E3FEDE49F)
- [Turning Red](drafts://open?uuid=88725F97-A787-4D34-A235-DEEC5369E9C2)
- [Snap.as Embedded Image Display Issue](drafts://open?uuid=171E93E9-3222-4491-844E-EEF6DD0FFA35)
- [I Finally Reached Computing Nirvana. What Was It All For      WIRED](drafts://open?uuid=4427754F-FFB9-4C27-A907-CE88CA6D7004)
- [Drafts Raindrop Collection](drafts://open?uuid=8B5A2F0A-5FCE-4A8F-817B-AB61C7019C86)
- [Current Draft Info Action](drafts://open?uuid=BBFDC458-1049-4FA3-A506-DE6C397B4C63)
- [Twitter Muted Words](drafts://open?uuid=B5A34BE2-DE76-4B3E-B5D5-4D3620519218)
- [Drafts Keyboard Shortcuts (iOS Export)](drafts://open?uuid=880E8C09-09D6-4712-B33F-D0D1630DDC3C)
- [Drafts Keyboard Shortcuts](drafts://open?uuid=F55AB610-D9F9-49D1-9C20-1CF6063DF03E)
- [Gap 1997 (Cassette Rip)](drafts://open?uuid=14774725-923B-4C82-8947-28CA651C838B)
- [Mac Menu Bar Shortcuts](drafts://open?uuid=68489605-5A2E-4250-8B8C-EC651CAEA449)
- [Mac Menu Bar Shortcuts](drafts://open?uuid=68489605-5A2E-4250-8B8C-EC651CAEA449)
- [BBEdit Learning](drafts://open?uuid=816D6041-B902-4E3D-9441-39D77C3748BC)
- [Drafts Twitter List](drafts://open?uuid=355A0209-6184-4FA5-B33D-F70AE50867B1)
**_[Drafts Twitter List](drafts://open?uuid=355A0209-6184-4FA5-B33D-F70AE50867B1)_**

- [Tim Nahumck](https://twitter.com/nahumck)
- [Drafts](https://twitter.com/draftsapp)
- [Greg Pierce](https://twitter.com/agiletortoise)
- [Brett Terpstra is fully microchipped](https://twitter.com/ttscoff)

The Nineties - Chuck Klosterman (Excerpt)
#reading #$
 The Nineties began on January 1 of 1990, except for the fact that of course they did not. Decades are about cultural perception, and culture can’t read a clock. The 1950s started in the 1940s. The sixties began when John Kennedy demanded we go to the moon in ’62 and ended with the shootings at Kent State in May of 1970. The seventies were conceived the morning after Altamont in 1969 and expired during the opening credits of American Gigolo, which means there were five months when the sixties and the seventies were happening at the same time. It felt like the eighties might live forever when the Berlin Wall fell in November of ’89, but that was actually the onset of the euthanasia (though it took another two years for the patient to die).  When writing about recent history, the inclination is to claim whatever we think about the past is secretly backward. “Most Americans regard the Seventies as an eminently forgettable decade,” historian Bruce J. Schulman writes in his book The Seventies. “This impression could hardly be more wrong.” In the opening sentence of The Fifties, journalist David Halberstam notes how the 1950s are inevitably recalled as a series of black-and-white photographs, in contrast to how the sixties were captured as moving images in living color. This, he argued, perpetuates the illusionary memory of the fifties being “slower, almost languid.” There’s always a disconnect between the world we seem to remember and the world that actually was. What’s complicated about the 1990s is that the central illusion is memory itself.
[The Nineties by Chuck Klosterman: 9780735217959](https://www.penguinrandomhouse.com/books/557048/the-nineties-by-chuck-klosterman/)

**[Music Quiz Siri Shortcut](x-apple-reminderkit://REMCDReminder/AF1796EF-ED39-4E56-970E-7C328EC17659)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[iMessage Group Chat Backup Thu, 05:49:44](x-apple-reminderkit://REMCDReminder/FBB4884F-E6C4-4AFF-A178-89E448028F27)**
`Todoist
Family`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[Siri Shortcuts for Scrubs](x-apple-reminderkit://REMCDReminder/DF11C6E6-B27B-411C-A78B-646AFBB88586)**
`Todoist
Family`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[Type to Siri](x-apple-reminderkit://REMCDReminder/4262C028-5650-48F8-AA06-A9C227D5F6F8)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[“DefenderShield”](x-apple-reminderkit://REMCDReminder/FEDA0145-4E60-465D-9285-CC28125F63C4)**
`Todoist
Family`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[Legacy Contacts](x-apple-reminderkit://REMCDReminder/ADA4BD51-51D5-49BE-BB81-739E02827620)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[[iPhone 13 Pro vs Pixel 6 Pro: 2000 Photos Later](x-apple-reminderkit://REMCDReminder/63CCAC7C-2D1E-4B97-9FD7-DCEB28F048E8)** 
`Todoist
Family`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 

 Full Frame Ep. 5 - YouTube](<https://m.youtube.com/watch?v=eAxbBbi_Mmw>)

**[Installing Custom Ringtones](x-apple-reminderkit://REMCDReminder/DEB30FA5-7E58-40CC-8836-ECA455369390)**
`Todoist
Family`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[[Apple’s Anti-Sexting Tool for iOS Will Warn Kids About Nudes—but Won’t Notify Parents](https://www.wsj.com/articles/apples-anti-sexting-tool-for-ios-will-warn-kids-about-nudesbut-wont-notify-parents-11638626402) ](<x-apple-reminderkit://REMCDReminder/3295A823-B167-43F3-809B-8F3848ED1E17>)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[Light/Dark Shortcuts](x-apple-reminderkit://REMCDReminder/8E61CFCF-FCB7-41FF-A3B7-CAB1063A9D39)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[iOS 15.2 Update](x-apple-reminderkit://REMCDReminder/14A7208F-632D-4EC0-BA3E-407E0A0A9AFF)**
`Family
Todoist`
[Link]()
Created: Dec 7, 2021 at 22:44
Due: 



**[How to share notes from Mac to work together](x-apple-reminderkit://REMCDReminder/3CFF2336-83D3-412D-AA4B-DCE8C3630373)**
``
[Link](https://setapp.com/how-to/share-notes-on-mac)
Created: Dec 17, 2021 at 01:17
Due: 



**[On Noir](x-apple-reminderkit://REMCDReminder/549414E1-7238-41DA-ADA3-4A6EA08318E3)**
`Curation
Style
iOS
Family`
[Link](https://tools.applemediaservices.com/app/:appId)
Created: Jan 8, 2022 at 10:06
Due: 



**[Native Apps Poll](x-apple-reminderkit://REMCDReminder/4D6EC62B-80B6-4605-8511-5FA02B413A52)**
``
[Link]()
Created: Jan 8, 2022 at 17:36
Due: 



**[iOS Custom Pronunciation](x-apple-reminderkit://REMCDReminder/16BBA3BE-4D30-4725-B38D-CF4F5532E9F1)**
`TTS
Guide
Accessibility
Documentation
Audio
Family`
[Link]()
Created: Jan 28, 2022 at 18:28
Due: 



**[REVIEW THE IPHONE USER GUIDE](x-apple-reminderkit://REMCDReminder/ECA5DAC7-AC53-406A-8383-F32399D52AB3)**
`Keys
Configuration
iOS
Documentation
Correspondence`
[Link](https://books.apple.com/us/book/iphone-user-guide/id1515995528)
Created: Sep 21, 2021 at 20:48
Due: 



**[🅵🆄🅻🅻](x-apple-reminderkit://REMCDReminder/B493DA90-3D09-402B-8845-9E14DDE707FF) [🅺🅴🆈🅱🅾🅰🆁🅳](x-apple-reminderkit://REMCDReminder/B493DA90-3D09-402B-8845-9E14DDE707FF) [🅰🅲🅲🅴🆂🆂](x-apple-reminderkit://REMCDReminder/B493DA90-3D09-402B-8845-9E14DDE707FF)**
`Configuration
FUCK
Dev
Keys
Documentation
iOS`
[Link]()
Created: Sep 24, 2021 at 17:38
Due: 



**[Add Telegram Widget to Landing Page](x-apple-reminderkit://REMCDReminder/4049EA4A-ABCE-4430-B167-B04CC9378F59)**
`Web
Social
Dev`
[Link](https://t.me/uikeycommand/6)
Created: Sep 27, 2021 at 16:25
Due: 

```
<script async src="<<<<<<<https://telegram.org/js/telegram-widget.js?15>>>>>>>" data-telegram-post="uikeycommand/6" data-width="100%" data-color="DA2573" data-dark="1"></script>
```

**[Keyboard shortcuts + quick jumps | Voters | Raindrop.io](x-apple-reminderkit://REMCDReminder/23987143-AD9F-4551-B746-A7321EAED17B)**
`Beta
Dev
Keys
Curation
Hardware
iOS`
[Link](https://raindropio.canny.io/feature-requests/p/keyboard-shortcuts--quick-jumps)
Created: Dec 11, 2021 at 17:03
Due: 



**[1 Image](x-apple-reminderkit://REMCDReminder/3C4EF9E7-112A-44E5-81B9-F798A3B44955)**
``
[Link]()
Created: Dec 17, 2021 at 04:35
Due: 



**[Messages for MacOS Keys Documentation](x-apple-reminderkit://REMCDReminder/EDDA21BE-EC7F-46E6-98F7-D265ADE34F05)**
`Shortcuts
Documentation
MacOS
Keys`
[Link](https://www.icloud.com/numbers/0254dhW3rpt8C_NBx7e0L9Vjw#Messages_for_MacOS)
Created: Dec 25, 2021 at 21:40
Due: 



**[Keyboard shortcuts on iPad? - General Discussion - Drafts Community](x-apple-reminderkit://REMCDReminder/91E8357F-8449-4E5B-AB5D-0CDFC6D01C32)**
``
[Link](https://forums.getdrafts.com/t/keyboard-shortcuts-on-ipad/10389/9)
Created: Jan 8, 2022 at 22:48
Due: 



**[iOS Keyboard Suggestions - Software - MPU Talk](x-apple-reminderkit://REMCDReminder/E3A27221-D71A-4882-A6D3-6C774C8750E1)**
``
[Link](https://talk.macpowerusers.com/t/ios-keyboard-suggestions/26922)
Created: Jan 10, 2022 at 03:55
Due: 



**[Full Keyboard Access in #MacOS](x-apple-reminderkit://REMCDReminder/869E2B56-BC0A-4642-9A83-1C6B40C0375E)**
`MacOS`
[Link]()
Created: Jan 10, 2022 at 22:44
Due: 



**[Typeeto: remote BT keyboard](x-apple-reminderkit://REMCDReminder/48D968FB-73E1-4DCF-AC4C-581FA74AA7C8)**
``
[Link](https://apps.apple.com/us/app/typeeto-remote-bt-keyboard/id970502923?mt=12)
Created: Jan 12, 2022 at 03:11
Due: 



**[Register a new account | Hackage](x-apple-reminderkit://REMCDReminder/9D9075D7-E296-4DC4-AE8D-1A40BA46EE12)**
`Dev`
[Link](https://hackage.haskell.org/users/register-request)
Created: Jan 18, 2022 at 23:39
Due: 



**[Announcing Windows 11 Insider Preview Build 22543 | Windows Insider Blog](x-apple-reminderkit://REMCDReminder/21FCF5DC-56BB-4573-9A4E-AA046D4A79EA)**
`Accessibility
Windows
Audio`
[Link](https://blogs.windows.com/windows-insider/2022/01/27/announcing-windows-11-insider-preview-build-22543/)
Created: Jan 27, 2022 at 18:41
Due: 



**[Keyboard Access Broken in IOS 15 | AppleVis](x-apple-reminderkit://REMCDReminder/7840BB0C-2324-48C5-9539-48E5DC1E2B06)**
``
[Link](https://www.applevis.com/forum/ios-ipados/keyboard-access-broken-ios-15)
Created: Mar 25, 2022 at 10:47
Due: 



**[Editorial Considerations List](x-apple-reminderkit://REMCDReminder/D6D53132-BB53-487B-A367-5BF41D090AE8)**
``
[Link](https://twitter.com/NeoYokel/status/1477105624978755584)
Created: Dec 31, 2021 at 20:36
Due: 



**[Tot Snippet Group Public Sharing](x-apple-reminderkit://REMCDReminder/1C2AF6F6-8E19-4D9C-A439-BCD9F62BFC6C)**
`MacOS
Configuration
Tot
Dev`
[Link](https://app.textexpander.com/group/171AB572-12EC-4CD9-A14F-B62927C3AD7D/sharing)
Created: Feb 2, 2022 at 01:41
Due: Mar 28, 2022 at 00:00



**[Development Progress: Remark.as](x-apple-reminderkit://REMCDReminder/37F318E8-18C3-4031-8002-0B21A46FD7FE)**
``
[Link](https://discuss.write.as/t/development-progress-remark-as/4031)
Created: Feb 12, 2022 at 14:09
Due: 





---
- [Notes-TildeTown on iPhone with Blink Shell](drafts://open?uuid=31122181-5565-4F1C-BEA3-C22DA140619A)
- [End User Episode 9](drafts://open?uuid=69EA3CC6-7081-47B4-B4AB-C16E5C7E8417)
- [Please Fuck With This License](drafts://open?uuid=FAFFA333-34A5-4412-B46C-3D216C09FD8E)
- [The Psalms Action Group](drafts://open?uuid=7580294A-E97B-4614-AAF6-4E4A63358E6E)
- [A Good Place: Periscope Magic](drafts://open?uuid=4B149084-B41C-4C82-8FD2-0FB7CCF67B6B)
- [Notes-Periscope Magic](drafts://open?uuid=480EF899-1228-4751-BFA2-1F197BF1B71A)
- [On Drafts and Obsidian](drafts://open?uuid=56EED87E-678E-4248-8E94-31650615C69A)
- [psalms - 03732022-094831](drafts://open?uuid=80C14B4A-CA47-4DFE-8BC5-7200E9561D09)
- [How to Fuck Text](drafts://open?uuid=A350D578-1CFA-4B5A-8655-74F6B3AD72AE)
- [Notes-Threema](drafts://open?uuid=994F9F9A-A333-43AA-AB54-0E9485025009)
- [David Blue's Twitter History](drafts://open?uuid=F490F4FE-5635-4513-9320-B5DB7E0FFE31)
- [Notes-Personal Twitter History](drafts://open?uuid=37B865E1-4372-4AF7-BB77-3979836130D4)
- [Reading](drafts://open?uuid=167BF51A-2C5D-4A40-8656-36AF81BB899E)
- [Tilde.Town on iPhone with Blink Shell (Attempt 1)](drafts://open?uuid=E43D7483-F933-47DE-989C-BFD1D6931C1A)
- [Colophon](drafts://open?uuid=E3CCA1D3-CD96-4A88-8A05-6598C86708A9)
- [Vocabulary](drafts://open?uuid=C41AC4C1-6F45-47C0-ACB2-74DDEDA8B41F)
- [David Blue Styleguide](drafts://open?uuid=2EB3DF66-8154-4201-ADC7-4F200D7A8A2F)
- [undercovered](drafts://open?uuid=A8DDF7DC-0D9C-4162-B028-5D8E75B819E5)
- [Principles of Text Fucking](drafts://open?uuid=194FF296-B132-4A15-A788-1BFDE5D380B9)
- [Hold Your Clipboard Managers Close](drafts://open?uuid=81CC3D50-BC7A-471F-B247-DBE0ACC54C6B)
- [Notes-Hold Your Clipboard Managers Close](drafts://open?uuid=65AC8211-F3F9-419D-A7B3-EBF82E4944F1)
- [Run Siri Shortcuts with Hyperlinks](drafts://open?uuid=A5A987C6-8978-4F4B-9FE1-D7C5408E1FD2)
- [Apple Rag Review](drafts://open?uuid=C2166752-61DC-446A-B833-96D156B75C00)
- [App Store Review Day](drafts://open?uuid=269CF0B7-74F8-4C12-8727-EA4968255DB0)
- [Notes-Run Siri Shortcuts with Hyperlinks](drafts://open?uuid=CF37457E-7D4D-43FE-8439-55AF4047AFB7)
- [‽‽‽](drafts://open?uuid=B9189A80-9FAB-4B82-8413-9AA29FBC0F5A)
- [Telegram Extras](drafts://open?uuid=F8A964A8-E7B2-44DA-B3A8-1832C485F3AD)
- [Bandcamp: Streaming's Secret Savior](drafts://open?uuid=DDCA42F5-BE83-43CB-99CA-7D527CAF926D)
- [Windows Eternal Drafts Theme](drafts://open?uuid=67DEB30C-4F91-4531-AFEE-10047D887203)
- [The Fastest Route to Twitter Jail](drafts://open?uuid=00666FB0-F0CB-48CF-9E9C-837435028D80)
- [“How to Fuck Text”’s Purpose Statement](drafts://open?uuid=19541905-994D-4BD6-9BD4-C4A511697546)
- [Notes-Twitter Jail 2022](drafts://open?uuid=00DDA3A9-9A34-40A2-A3F6-C619668D08F3)
- [psalms - 02442022-222718](drafts://open?uuid=D28A64BD-2090-4B3C-8E47-5A378F4C171C)
- [Notes-Using Drafts With NeoCities](drafts://open?uuid=70C7C7DF-5083-411A-A397-A14479CA90BF)
- [Using Drafts with NeoCities](drafts://open?uuid=7247282E-340B-4890-A2F7-0481AF31321E)
- [Drafts vs. Obsidian Discord Message | MacStories](drafts://open?uuid=C75F02AB-3DF2-4382-A85C-618B0A78C611)
- [Notes-Paste Shortcuts for Mac and iOS](drafts://open?uuid=59314DD1-4DC2-4835-B47C-817D125D2EEE)
- [Paste Shortcuts for Mac and iOS](drafts://open?uuid=F8426047-D514-44CE-8989-81FAB489507A)
- [Zalgo Text in a Keyboard Shortcut](drafts://open?uuid=775BA8C5-09A4-47C3-9B88-48D923BDB55D)
- [Notes-Shortcuts for Paste](drafts://open?uuid=D459A766-03AD-44C7-BFD9-1B09D49C3A5B)
- [Text Fucking Wiki Entry](drafts://open?uuid=DA06E944-7601-43D9-9D01-5AC0835804EE)
- [Notes Template](drafts://open?uuid=DEB4C792-DA8B-4965-AACE-E319E6588422)
- [On Configuration Culture](drafts://open?uuid=C06D5FA1-F9BD-45D6-B0E6-F61A648589B8)
- [The Psalms’ 2021](drafts://open?uuid=CB2985C0-C484-48F0-A170-2EBEF1E94972)
- [Assorted Unlisted David Blue Configurables](drafts://open?uuid=AC162701-9C53-449E-82AF-162581EB332C)
- [Remark.as is Now Enabled on This Blog](drafts://open?uuid=1FEB4A37-519C-4FE4-92FE-9A280459E4D5)
- [Remark.as Integration](drafts://open?uuid=B0C95CA5-E5DE-4478-9C09-1E30499438CE)
- [Notes-The Psalms’ 2021](drafts://open?uuid=E7E2670F-0DB1-49A1-BA26-73F3B43ACB52)
- [Automating Write.as Posts on macOS](drafts://open?uuid=DC7A1B0D-1F71-445F-8813-3EF9F0BC3967)
- [Notes-Text Replacement](drafts://open?uuid=AE17D11E-A3EC-40EF-9EAC-300ACDE4FFA8)
- [Text Replacement](drafts://open?uuid=42D45533-F9BB-4A73-8AA0-6EF2CEDFCE7D)
- [Notes-Telegram Extras](drafts://open?uuid=176B5140-D3F5-4C11-8829-3FD733B63B8E)
- [Underdocumented Maneuvers in iOS](drafts://open?uuid=6271A84E-D0F9-4881-A20D-B22E88AA9E3B)
- [Declare a Personalized Vocabulary across Apple’s OSs with Text Replacement](drafts://open?uuid=5FF0FAC4-21A2-4D56-8195-312C66B6E97E)
- [Assorted Birthday Configurables](drafts://open?uuid=5086A314-FEAB-45EA-AE55-28A2FC2D294E)
- [Notes-Assorted Birthday Configurables](drafts://open?uuid=85A87717-8D95-4AAB-8FEE-6F4B6FD8BEB8)
- [Against All Strategic Social](drafts://open?uuid=E33DD0DF-7BE5-4EF2-ACE8-3CAE8F39A969)
- [Notes-Fresh Eyes on macOS](drafts://open?uuid=6C76EDBD-025D-40BA-9381-9AE95AD70F36)
- [The Psalms Template](drafts://open?uuid=01287039-1517-4B58-8389-6D742BEC87A7)
- [Fresh Eyes on macOS](drafts://open?uuid=944F772C-CC9D-47F2-BD98-F4E435E40227)
- [Drafts Forum Profile](drafts://open?uuid=6406A595-E9B5-4FAD-BD29-725FC7CA43D6)
- [Speedy Frames](drafts://open?uuid=0E9AAECF-EDBB-4FD4-B3BA-2CFC55D7BA30)
- [I Trust Telegram (Final Web Duplicate)](drafts://open?uuid=6A402155-C4DE-4D2D-8372-F6B1810C8943)
- [Telegram-References](drafts://open?uuid=C1F015BD-D186-4570-B40B-B3EB68471BC9)
- [Notes-Telegram](drafts://open?uuid=DEDF0625-BCAF-40BE-8C15-DFCEE029A182)
- [Speedy Frames Shortcut Documentation](drafts://open?uuid=B1198509-030F-4526-9721-A0FB85A799BA)
- [2021 PERDITION Icon Set](drafts://open?uuid=AEAE1363-2E50-4FB1-B9C6-0C55620A1EC5)
- [GitHub URLS](drafts://open?uuid=F8978B8E-2242-49EF-AE09-41512DAEFE6E)
- [Petrify Vanity Code Renders for The Psalms Repo/Colophon](drafts://open?uuid=A181CA53-D79E-4FFE-A7FC-E98DBEB9A200)
- [About](drafts://open?uuid=1F0A8F2F-FF24-4972-BBDE-9A95B3B2B97C)
- [David Blue on Vine: The Platinum Collection](drafts://open?uuid=393EC29E-A49D-4126-AAB4-055DB3104D20)
- [WordPress/Aviary Prank Shot](drafts://open?uuid=36C0D7DF-3B5B-4E0F-A96A-9250B6066CA0)
- [Resisted Abstraction](drafts://open?uuid=49767CE5-0366-4760-9542-8CE454DD524D)
- [Duplicate - I Trust Telegram](drafts://open?uuid=5D1347E7-B1ED-41EA-9CF9-100A8F5561B5)
- [Audio File Example Siri Shortcut Documentation](drafts://open?uuid=053A02C7-D314-46B2-B1F7-CFD2BDB65E7D)
- [The Psalms’ 2021](drafts://open?uuid=A6E02F34-FAFF-46FF-AE16-6710788D2FC9)
- [Notes-Siri Shortcuts for Scrubs](drafts://open?uuid=93B5ECAF-90DD-4FE5-A623-1A50B66DA316)
- [Your thoughts/questions/complaints/outbursts about Twitter Blue - Twitter Space](drafts://open?uuid=FF7C822D-E5BF-49C7-8AC3-1F042A80F7ED)
- [The Psalms' 2021 (Collected)](drafts://open?uuid=CB8FD41B-385E-488C-A811-D4222905FF1B)
- [The Psalms Template](drafts://open?uuid=524F72B0-A993-4EC6-8377-974E4856BE8D)
- [The Psalms Template](drafts://open?uuid=56FEF823-A9DF-46F3-BDFB-A23C0273AE8F)
- [Fantastical for iOS Keyboard Shortcuts](drafts://open?uuid=21736FC6-D738-4CF8-8AA0-6AE34A78692E)
- [I Trust Telegram (Chaffback) Wed, 08 Dec 2021 03:59:02](drafts://open?uuid=CD26F1A7-A03F-401A-8F9D-72117AC05722)
- [Reading](drafts://open?uuid=3D0DB72E-91B1-4299-BAF1-84171EC4DD56)
- [The Psalms Todoist Import](drafts://open?uuid=259F4DD0-CBA0-451F-91E1-9B4B3AED692B)
- [David Blue on Twitter Blue](drafts://open?uuid=E1DA2728-335E-43CA-94FE-B9E954B17DC9)
- [Duplicate-Blue on Twitter Blue](drafts://open?uuid=1FC3706C-677B-4A4B-83F9-037823BD857B)
- [Twitter Thrives on Incompetence](drafts://open?uuid=B9A0D30A-EAF4-4CF4-A5E9-B68403A9E146)
- [For Print-Blue on Twitter Blue](drafts://open?uuid=8281A612-6D72-4103-92CE-A548C995E728)
- [Mobile Dot Twitter Dot Com](drafts://open?uuid=280D306E-58A5-4C4A-8D05-DE77252A4AEA)
- [Now That’s What I Call David Blue’s 2021 Playlist](drafts://open?uuid=7020E3C2-A0BA-45F8-8A10-B2B4341BAD83)
- [Siri Speech Synthesis (MacOS)](drafts://open?uuid=1EC8FDDC-0D61-4B47-AA74-C1E07D784072)
- [The Psalms' Title Represents a God Complex - Swaggatronix on Mastodon](drafts://open?uuid=155702CD-30E3-4633-B9ED-546D94507590)
- [Shortcut Template Backup](drafts://open?uuid=141B7123-2ACC-464B-8E79-0A1DDA11EF7C)
- [They/Them Siri Shortcut Documentation](drafts://open?uuid=CC4A0017-EDF9-4837-83AC-FC3785799D3E)
- [Notes-YouTube Music](drafts://open?uuid=10B3C676-864F-4FA8-B7D2-473A3BBCCB15)
- [Notes - Min Browser Review](drafts://open?uuid=C08A29C7-39AF-4CEC-A54B-09EA663AD450)
- [Min Browser Review](drafts://open?uuid=D2112906-EF4C-498A-BA4E-FF4172C2A47E)
- [I Bought a Desktop](drafts://open?uuid=6F3E65B1-D198-49E2-A764-F22F3B99172D)
- [Blessed Web Utilities Collection](drafts://open?uuid=BF3739DD-D91B-4BF2-84F5-30B08501F69E)
- [What’s wrong with Spotify? Siri Shortcut Documentation](drafts://open?uuid=72B8330D-64D6-4CCE-9795-86EE439B00BE)
- [Timelines - Raindrop Collection](drafts://open?uuid=76439481-25B8-47E2-841D-96DC4822C8B3)
- [Truth Social -261](drafts://open?uuid=25DC16BA-0387-4169-8137-CFC34E627D8C)
- [The Extraordinary Intimacy of Microblogging](drafts://open?uuid=85C4FB1C-1C6A-4122-8A58-AC2380122EB3)
- [Telegram Guides](drafts://open?uuid=4DFE480F-7A82-4B97-B302-56605F19B857)
- [PocketCasts Native Podcast List](drafts://open?uuid=4D130E4A-FFE7-48B1-9A0B-D9797C3C054A)
- [Min Discord](drafts://open?uuid=F00A6748-E914-4F2E-9495-A1408248C7FA)
- [Notion Library](drafts://open?uuid=AD37EA0F-230F-4ADD-AEA0-A5666C7677D5)
- [Views of The News on HTML Parsons](drafts://open?uuid=B96B0977-794E-4B86-9084-80D1B5FFE879)
- [FUTURELAND (Newsletter Revival) Documentation](drafts://open?uuid=36320115-76E1-46C2-BDEA-CD904BE5C9E4)
- [Siri Shortcuts for Scrub     s](drafts://open?uuid=329BC931-CADD-4381-9D42-A2C03D7B1047)
- [Easily Converting Apple News Links to Their Original URLs](drafts://open?uuid=CA641427-0757-4D54-AAF2-CF2FB8EFDFA3)
- [I Trust Telegram](drafts://open?uuid=7C27AD95-762F-4A9B-9D3B-6FBD98FEA237)
- [SI on Discord Regarding Objective-C and Swift](drafts://open?uuid=BBDCD568-C0AF-4561-9652-CA32F449DFE5)
- [Windows 11 Family Message](drafts://open?uuid=EA14CC00-5C18-4C56-AC9B-24F5A30882BB)
- [I Trust Telegram](drafts://open?uuid=8F8F0F01-8C5D-4D47-94EC-B72237EDF3B7)
- [“”””Privacy”””” (Telegram)](drafts://open?uuid=227AA173-41D9-4ABF-BF64-FB9354C7F6A0)
- [Highlights x Hypothes.is Feedback](drafts://open?uuid=1663735D-E824-48EB-AEE8-362B6C4E17B3)
- [Opera for iOS App Store Review](drafts://open?uuid=24AE5863-158D-4079-A838-824C97E99F9A)
- [/t Repo README](drafts://open?uuid=473BE92C-D29C-4231-ACA7-DAB9B62FD1F5)
- [SYSTEM COLORS Telegram Themes](drafts://open?uuid=0C63E37F-63F3-4F19-93DA-F9EA64C84779)
- [The Pinterest Machine - MacStories Discord](drafts://open?uuid=0C3DF863-351F-4BA2-AD52-BE8D873E57A6)
- [* “[Using Drafts with Obsidian - Integration Guides - Drafts Community](https://forums.getdrafts.com/t/using-drafts-with-obsidian/11221)”](<drafts://open?uuid=15486ACE-3C5B-454F-84FF-BA85FED2A5A8>)
- [Safari Extensions for iPhone](drafts://open?uuid=54024A7C-B838-49CD-A2C0-829DE435FF5F)
- [Radical Minutia Social Post](drafts://open?uuid=3E642E2C-4E6D-4E11-8275-B2F115683792)
- [The Agony & The Ecstasy of the iPhone 12 Pro Max](drafts://open?uuid=07B641B2-6692-4FA9-9E7C-79D7EED5F803)
- [Notes-Circling Back to Tweetbot 6.3](drafts://open?uuid=DC113858-DC00-4130-90DD-584B6CADD535)
- [Chill](drafts://open?uuid=2D5A8213-A4D5-4634-B10F-81ECEBF83808)
- [![Mastodon for iOS](https://i.snap.as/sHTDx8AV.png)](<drafts://open?uuid=502AAEF8-402B-4E32-A43B-AD997F3815CB>)
- [Circling Back to Tweetbot 6.3](drafts://open?uuid=9DFCF44B-AEC9-4BE5-82AA-1506EFBBF3E3)
- [Circling Back to Tweetbot 6.3](drafts://open?uuid=90E530C2-52DC-4AF2-9855-065C0052BA93)
- [Jorts Readme](drafts://open?uuid=DC0381BC-394A-4F43-8723-0C0FDC76D2BE)
- [Safari](drafts://open?uuid=0C3B8273-BC35-44E6-8D8B-681A270C0962)
- [iOS 15’s Last Bullets](drafts://open?uuid=8757F54B-420D-491C-BCD2-A33FFD6F99F3)
- [Drafts 28 Bluetooth Keyboard Considerations](drafts://open?uuid=F74B3E82-8A13-445D-B0DD-0EF2ED52773E)
- [Drafts 28.0 Release Notes](drafts://open?uuid=A7AA40AC-664F-49ED-9381-D6ABEB1C6681)
- [Embed <audio> Element:](drafts://open?uuid=E3D8E967-1822-4D60-8CC0-BCA31A1861F4) [⇧⌃A](drafts://open?uuid=E3D8E967-1822-4D60-8CC0-BCA31A1861F4)
- [NOTES: Underdocumented Maneuvers in iOS](drafts://open?uuid=464F92D4-5A63-4447-BBF7-CB1B0D885568)
- [Twitter Thread:](drafts://open?uuid=101762F8-538A-454D-9049-F6E391483EC7)
- [Shortcuts User Group Transcript](drafts://open?uuid=7FD1C728-9A20-4993-8D35-C1837A49BC62)
- [Jeanne, 27](drafts://open?uuid=AC0B2868-8566-4463-999C-89BE131141FB)
- [Notes: iOS 15](drafts://open?uuid=1AA6A947-6F19-40A0-BC10-C58D5A5D6292)
- [Timestamp Share Feedback-Whyp](drafts://open?uuid=03D3779A-3438-405C-9443-05157D7CC423)
- [Widgets](drafts://open?uuid=23BC9164-96F2-47AD-8461-BFF89B57B622)
- [Tweetbot 6.3 Keyboard Shortcuts](drafts://open?uuid=79E69D39-C826-4F36-873A-FCDF26E279D6)
- [An Overview of the iPhone 13 Event for The Living](drafts://open?uuid=FAC9FD1F-AE9E-46A8-B8FB-4934A53CBFF4)
- [Weather](drafts://open?uuid=221ED845-37A6-4443-9ABA-0551C4A20D49)
- [Overflow](drafts://open?uuid=6B55D761-4FCA-4553-9446-E2B956ADF549)
- [iOS 15 MacStories Discord Message](drafts://open?uuid=1F1EFADA-2284-4172-B429-CEB70F2CEE09)
- [iOS 15 Reviewed for My Family 2021-09-14-04.29.08 Backup](drafts://open?uuid=31C3D084-49AE-4677-ACCF-BD51AF09CBEE)
- [/\* Written in November 2020 by David Blue for bilge.world.](drafts://open?uuid=71AFC3F0-F8BF-490D-914C-A1F5470A1BE9)
- [Reminders Backup Shortcut Documentation](drafts://open?uuid=2E7B3F43-84D3-4043-8CDD-02B03E8BAB15)
- [Audio Recording Apps](drafts://open?uuid=FA42C451-169C-4E83-96BF-321B5CD729D6)
- [MacStories Discord General Correspondence](drafts://open?uuid=FAB72C67-5029-4423-9C3B-A7BF584237FC)
- [Coda: A marketing exercise](drafts://open?uuid=F1269349-39EB-4956-AAEE-29A0CD3F8D38)
- [Siri Speech Synthesis in iOS 15](drafts://open?uuid=68604C9C-726E-48AD-879C-1E0A67FBBF11)
- [Siri Speech Synthesis Shortcut Real Time Demo [YouTube Meta]](<drafts://open?uuid=55949278-704D-40ED-9973-38E5F650F868>)
- [[The Unlicense, Dave Edition](https://gist.github.com/extratone/140a11428b5dd1dda500b3928e0438b1)](<drafts://open?uuid=B0B07AAF-F5A7-47BC-A66D-6B8B99890F21>)
- [Make Audio from Document Text](drafts://open?uuid=480B554F-3C8D-4F76-B7B9-41D1E939E020)
- [Family Tech Support Intro](drafts://open?uuid=3556D063-194D-4E5B-8B4C-25A13996766D)
- [iOS 15 Extras](drafts://open?uuid=98E9F3BA-40C4-4439-8F99-083F31BBA47C)
- [[Take 2] iOS 15 Reviewed for My Family](<drafts://open?uuid=32400DD0-9321-4C09-AEF9-BBA8CAA1F9BB>)
- [You can find me in HyVee, bottle full of bub](drafts://open?uuid=4284D663-82ED-4E2B-AD7D-15CB1F2CA8E9)
- [The Platinum Clacker Award (Tweetbot)](drafts://open?uuid=226EFAAC-C7AC-4E95-BBAD-153DCA6DD60C)
- [Make Audio from Article Body](drafts://open?uuid=81B20B69-6676-4973-AA8C-692812D89399)
- [Notes - Siri as Text-to-Speech in iOS 15](drafts://open?uuid=CA396A88-94DC-4798-B48F-9F8D1772BC3A)
- [Developer Forums Keys Question](drafts://open?uuid=7A41ACA1-0395-4E11-9B2F-09AAE0D0A84D)
- [Against All Strategic Social](drafts://open?uuid=541AA9C1-630C-4508-94DC-043ACF10A916)
- [Transcript - Gargron is Not a Growth Goblin](drafts://open?uuid=B8850B72-54D9-4306-A062-00B7ED17F72F)
- [Transcript - The iPhone x Bluetooth Keyboard Issue is a Class Issue](drafts://open?uuid=1437B275-8F64-4229-9075-E44419ED19AD)
- [Notes: Social Media Documentation](drafts://open?uuid=DFF30543-34A4-4FF2-BD02-2629583729EC)
- [App Polygamy](drafts://open?uuid=7BE90BE5-F95B-4F5A-BBA6-CA2EAB663236)
- [Notes - Mastodon for iOS (The “Official” App)](drafts://open?uuid=DE4D9E6C-08EE-4CA1-B4F0-DD5332A6D64F)
- [Kelly Sims (@kellylsims) DM](drafts://open?uuid=FE37B9BA-DC59-4949-A463-078DF0AC379D)
- [Jackson Tan Cold Email](drafts://open?uuid=3583FCEC-AA7A-4F3D-86E8-5272600C804E)
- [Hi Mastodon. This is a darned Automated Post.](drafts://open?uuid=750115F5-C954-4ADE-9D2C-289CBFDB6346)
- [General Keys Considerations](drafts://open?uuid=75BD49B3-3382-44CA-8E6A-3AACE321C42F)
- [Store To-Review List](drafts://open?uuid=51532713-32E0-4E49-B589-55A2801BA273)
- [Ashley Carman DM Regarding Tinder Photo Tip](drafts://open?uuid=D958D73A-1D8A-491F-80F1-4BAC9C4F0AF8)
- [Raindrop](drafts://open?uuid=B53A6550-62AF-4C26-8AFF-6B31BE3D1979)
- [Notes - Preface](drafts://open?uuid=EBF75B28-1102-4F47-85EC-D9CF68607FD0)
- [The ExtraKeys Universal Clacker Award](drafts://open?uuid=863860F1-C4D8-4559-825A-21C86DCF868A)
- [Bluetooth Keyboard Section in the Official iPhone User Guide](drafts://open?uuid=016B77F0-F48D-4D36-A5A3-4C84FD27199F)
- [if you’re \*absolutely positive\* you want to know more, I’ve tossed most of my findings in this GitHub issue over the past few months: https://github.com/extratone/bilge/issues/79](drafts://open?uuid=0F035275-0916-455A-B9BC-E8D2A884BDBF)
- [Echoes in Twitter Spaces](drafts://open?uuid=3332A5DA-5001-4C81-B1DB-FC1580A7C91C)
- [WF Workflow Social](drafts://open?uuid=99864B0C-2D08-4760-979E-48A4CA4C0298)
- [Notes: Writeas/WriteFreely iOS Publishing Workflow](drafts://open?uuid=0A53E1CC-C30F-4690-A8E7-6F7CCB107879)
- [Duplicating My (Overwhelmingly Positive) Mastodon Experience](drafts://open?uuid=419D096A-D897-4305-9689-52554D3E90C4)
- [App Store Review Day](drafts://open?uuid=461912F9-A3F0-4EA9-9397-A910FDF349E2)
- [[Aviary for iOS Documentation](https://apps.apple.com/us/app/aviary/id1522043420)](<drafts://open?uuid=ECA9EB56-4FF2-4DE2-BF06-417BAE687103>)
- [Whodunitfirst (iOS 15)](drafts://open?uuid=F12F9A7F-CEA4-40DB-871C-E7334D8A6DA1)
- [[My iPad Setup](https://nipunbatra.github.io/blog/setup/2021/06/14/setup-ipad.html)](<drafts://open?uuid=058D531A-69F5-4218-93D2-D3395F380789>)
- [App Notes: The State of Mastodon Clients on iOS](drafts://open?uuid=955F55BA-F62D-4738-AB5E-3BC29F6C93F9)
- [Scan Thing Documentation](drafts://open?uuid=05E924FF-FA3B-4561-B429-3630142D3EC3)
- [“A table view that allows navigation and selection using a hardware keyboard.” | Swift Snippet by Douglas Hill](drafts://open?uuid=DFBC917C-2B4C-4222-B05B-45B49DBA42AA)
- [Mastodon Live Onboarding + Auth Marathon Meta](drafts://open?uuid=E7C8B4DB-1B08-4FB2-9AC1-5875C462DA5E)
- [Per-Browser Keyboard Shortcut Behaviors ~~Inconsistencies~~](drafts://open?uuid=7FB0D143-B87B-4BA7-BFE7-01B6DC87B4D3)
- [Golden Quran |](drafts://open?uuid=EC6968E8-88B1-412D-BF67-90A9F68E4FCB) [المصحف](drafts://open?uuid=EC6968E8-88B1-412D-BF67-90A9F68E4FCB) [الذهبي](drafts://open?uuid=EC6968E8-88B1-412D-BF67-90A9F68E4FCB) [Documentation](drafts://open?uuid=EC6968E8-88B1-412D-BF67-90A9F68E4FCB)
- [Continuing to Explore Social Ownership](drafts://open?uuid=3AD201C1-B1AF-4AF4-81FB-3037CA3E07FA)
- [Writeas/WriteFreely iOS Publishing Workflow](drafts://open?uuid=B06358D8-2659-4450-A7AE-27BA0705A27C)
- [Anecdote: The Unknowables of Modern Gasoline Dispensation](drafts://open?uuid=678B20C1-1D04-49C8-B29F-D273E1899CE1)
- [Zhiyuan Zheng's Portfolio](drafts://open?uuid=F4539A6D-ACFA-42F3-9C0C-87F2CCB4C5BB)
- [Capture Web Page in Markdown](drafts://open?uuid=47451873-C05C-45E3-B99F-F6C873899325)
- [![Marco! Banner](https://i.snap.as/D6SijbwF.png)](<drafts://open?uuid=A71289ED-A37F-4178-8523-747A56E1F14B>)
- [Imagining an Operating System Entirely Devoid of Automation](drafts://open?uuid=46B55726-1C06-4626-BE58-0E68AC87F17B)
- [Keyboard shortcuts on iPad? | Drafts Community Thread](drafts://open?uuid=3AD318B0-5DBF-4F4D-BE3C-C76C31D3B96B)
- [The Death of John McAfee](drafts://open?uuid=6CC2340E-5AE7-46BC-BEC1-561C5ED2A773)
- [App Notes: The State of Mastodon Clients on iOS](drafts://open?uuid=865C62F2-3C6F-40C4-A07D-EF4A65572A1A)
- [WWDC 2021](drafts://open?uuid=4E1FA4F0-01F6-4165-8439-5412B063C11B)
- [Notes: Podcasting’s Last Month](drafts://open?uuid=CE8DDCEC-FB0D-4FD5-B911-F234395E36E2)
- [![Big Boy; Big Phone](https://i.snap.as/KtqMF5Ky.jpg)](<drafts://open?uuid=4BD56000-A849-452E-9938-90128BBCC938>)
- [Map My Drive for iOS](drafts://open?uuid=E2CA272F-3642-4496-99FA-D3977576DD2B)
- [Notes: MapMyDrive for iOS](drafts://open?uuid=53F412B2-7352-455A-9D7B-94CFC8B9B936)
- [Transcript: “On ‘Identity’”](drafts://open?uuid=B642CF33-6AD4-40DB-8410-90FD0AF85AF6)
- [Notes: The State of Mastodon Clients on iOS](drafts://open?uuid=A831B815-C12C-4A00-BCFF-D88B7F0E05AC)
- [Why I Didn't Follow You Back](drafts://open?uuid=7E18A2E7-6F94-4196-BC7A-CFB380E3FCC6)
- [https://itunes.apple.com/us/app/id1415599567](drafts://open?uuid=13CE2D87-9951-464A-AABC-12750788C1AB)
- [A Nauseous Plea for iterative terminology](drafts://open?uuid=F3E54744-0952-4836-BF04-2C7A8520D47D)
- [I just discovered what a "Gist" is on GitHub](drafts://open?uuid=D10047DB-E6D6-4B36-8D36-8EC34B869525)
- [Pitching Input](drafts://open?uuid=25803989-24FF-4EF1-B5D6-51D3DAD61B65)
- [The Psalms' About Page Version 2.0](drafts://open?uuid=21EE648B-74B6-433C-BBCE-7281BB95F41C)
- [In Defense of Just Fucking Around](drafts://open?uuid=84D6A7BD-4A17-44F7-A31A-67BFB0F446FF)
- [Why I Write About Technology](drafts://open?uuid=40768A19-8D81-4A19-AC41-BCE3BC97C338)
- [Revelations in Web Starvation](drafts://open?uuid=213B9E76-EF2C-4B34-B7CA-341E694E7BD3)
- [Virtual Reality Virginity, Lost](drafts://open?uuid=46E9CED7-334D-4E2C-92DD-7AC6956A2CC0)
- [Google Will Soon Replace God and The Church](drafts://open?uuid=DF5E8457-53A6-4423-B125-54CC8BF88251)
- [Mark Fuck and the Goofy Godheads](drafts://open?uuid=DC58C318-F899-4165-9D7D-6F213F60D2C8)
- [Why I Write About Technology](drafts://open?uuid=B554E17B-5310-4DCD-8F03-CB56859E249D)
- [help](<shareddocuments:///private/var/mobile/Containers/Shared/AppGroup/837A7069-BE1B-4844-8AFB-CE151BFBBD11/File> Provider Storage/Shell/help.txt)
[symbolnames](shareddocuments:///private/var/mobile/Containers/Data/PluginKitPlugin/8F4DA708-2288-4F6D-8A46-894FA59CD852/tmp/03EFD5E8-A666-422F-8091-857760A245FB/symbolnames.md)
- [help](shareddocuments:///private/var/mobile/Containers/Shared/AppGroup/837A7069-BE1B-4844-8AFB-CE151BFBBD11/File%20Provider%20Storage/Shell/help.txt)
- [iOSColors](shareddocuments:///private/var/mobile/Containers/Data/PluginKitPlugin/8F4DA708-2288-4F6D-8A46-894FA59CD852/tmp/94810C12-FA62-4BAF-8543-C63D8FBA75F0/iOSColors.css)%0A-%20[iOS Soundboard](shareddocuments%3A///private/var/mobile/Containers/Data/PluginKitPlugin/8F4DA708-2288-4F6D-8A46-894FA59CD852/tmp/E408F64A-0E1F-4E88-B68E-654B1D8EA4F5/iOS%2520Soundboard.shortcut)
- [WFActionsplist](shareddocuments:///private/var/mobile/Containers/Data/PluginKitPlugin/8F4DA708-2288-4F6D-8A46-894FA59CD852/tmp/A3C5A16D-3064-4A22-AAAA-5851BB4DB503/WFActionsplist)

sftp <blue@odette.local>:/zsh.txt

cd Library/Mobile\\ Documents

imessage -t hi -c [+15738234380](tel:+15738234380)

ls -1 -d "$PWD/shortcutsrepo/signed/"\*

<https://davidblue.wtf/shortcuts/batchshortcutssigner-ios.html>
5

I modified Federico’s Shortcut Injector to sign multiple shortcuts at a time (by folder.) I was even able to get [an over-ssh version](https://routinehub.co/shortcut/11467) working, though actually getting the resulting files back to the device is (at the moment, at least) beyond me. <https://routinehub.co/shortcut/11467>
- Batch Shortcuts Signer (iOS): <https://routinehub.co/shortcut/11467>
- Batch Shortcuts Signer (macOS): <https://routinehub.co/shortcut/11401>


long1950-5G
`04932022-132932`
- Down: 191
- Up: 70
- Ping: 35
- RPM: 112
- Type: wifi

long1950-5G
`04932022-142931`
- Down: 271
- Up: 72
- Ping: 39
- RPM: 614
- Type: wifi

long1950-5G
`04932022-152935`
- Down: 151
- Up: 63
- Ping: 35
- RPM: 256
- Type: wifi

long1950-5G
`04932022-162935`
- Down: 242
- Up: 71
- Ping: 35
- RPM: 549
- Type: wifi

long1950-5G
`04932022-172929`
- Down: 262
- Up: 71
- Ping: 32
- RPM: 640
- Type: wifi

long1950-5G
`04932022-173001`
- Down: 263
- Up: 72
- Ping: 38
- RPM: 551
- Type: wifi
- [Shortcuts](shareddocuments:///private/var/mobile/Containers/Shared/AppGroup/837A7069-BE1B-4844-8AFB-CE151BFBBD11/File%20Provider%20Storage/Shortcuts.sqlite)
# [04932022-191309](tel:04932022-191309)

- [The Psalms Action Group](drafts://open?uuid=7580294A-E97B-4614-AAF6-4E4A63358E6E)
- [Action Group Index](drafts://open?uuid=411D556C-2ABF-4181-8D43-C6E46559BBEA)
- [Drafts Keyboard Shortcuts (iOS Export)](drafts://open?uuid=880E8C09-09D6-4712-B33F-D0D1630DDC3C)
- [Please Fuck With This License](drafts://open?uuid=FAFFA333-34A5-4412-B46C-3D216C09FD8E)
- [App Store Reviews](drafts://open?uuid=25628140-FC0A-4DBF-96BE-1E7624E9F50A)
- [Fantastical Learning](drafts://open?uuid=494AA9DA-843D-4A05-9F8D-EB126FB85EED)
- [David Blue‘s Drafts Configs](drafts://open?uuid=38A942A2-33C4-4630-9CDE-B005989F4E3A)
- [Notes-Siri Shortcuts and More for Tot](drafts://open?uuid=E6D127B8-B5CE-45F5-8D6A-0BC5CE12A149)
- [Siri Shortcuts and More for Tot](drafts://open?uuid=9DAFE8FC-3655-4FBD-BA16-7439BAC109FA)
- [Re: screaming](drafts://open?uuid=2DFBD718-8845-49CF-A343-8BA3B639B7B3)
- [David Blue’s WORLD FAMOUS Fast n’ Loose™ Siri Shortcuts for](drafts://open?uuid=68F049EC-59AF-4A49-885F-698BB9653400) [𝚂𝙲𝚁𝚄𝙱𝚂](drafts://open?uuid=68F049EC-59AF-4A49-885F-698BB9653400)
- [Dots](drafts://open?uuid=6CB89F33-7463-4201-89E4-F1D850008D8A)
- [End User Episode 9](drafts://open?uuid=69EA3CC6-7081-47B4-B4AB-C16E5C7E8417)
- [Drafts Keyboard Shortcuts to Re-Emphasize](drafts://open?uuid=55EDFC80-8307-4197-9A8F-754C1656A43F)
- [A Good Place: Periscope Magic](drafts://open?uuid=4B149084-B41C-4C82-8FD2-0FB7CCF67B6B)
- [Notes-Periscope Magic](drafts://open?uuid=480EF899-1228-4751-BFA2-1F197BF1B71A)
- [[Jayson](https://apps.apple.com/us/app/jayson/id1468691718) App Store Review](<drafts://open?uuid=A953A625-CB34-47B7-B944-FAD0B617966D>)
- [Birthday: Table of Contents](drafts://open?uuid=A5AEEC73-21C7-4ECA-B214-F459EF12AAC5)
- [gas station robbery https://t.co/TKZ20Ze9oh](drafts://open?uuid=A3881C9D-A11E-4367-BEA9-5F1E53C0FB57)
- [I.... have a PhD position? This is so cool!](drafts://open?uuid=C42778FB-2D9A-48B9-8195-248616062DC4) [🥳🥳](drafts://open?uuid=C42778FB-2D9A-48B9-8195-248616062DC4)
- [Twitter Search: @neoyokel](drafts://open?uuid=45F41B98-3F9F-473E-A1E1-6B44E1B7B729)
- [NeoCities/WebDAV Action Group](drafts://open?uuid=871D7EF0-8BAF-438B-8A5A-E3978B659FD1)
- [Hold Your Clipboard Managers Close](drafts://open?uuid=A7A3A08D-3C07-49E3-A6D0-A2DEF40DF974)
- [Please Fuck With This License](drafts://open?uuid=A61AB9CD-1756-4E05-B50B-5AB6AE693BE7)
- [David Blue’s WORLD FAMOUS Fast n’ Loose™ Siri Shortcuts for](drafts://open?uuid=DD9ED498-1B00-45F7-B610-86E4570DB9E2) [𝚂𝙲𝚁𝚄𝙱𝚂](drafts://open?uuid=DD9ED498-1B00-45F7-B610-86E4570DB9E2)
- [@BrianMueller333](drafts://open?uuid=82E0B364-962F-4711-AE16-701D0EED2234) [❤](drafts://open?uuid=82E0B364-962F-4711-AE16-701D0EED2234) [🫂](drafts://open?uuid=82E0B364-962F-4711-AE16-701D0EED2234)
- [Twitter Search: neoyokel](drafts://open?uuid=58D83DD4-3C0D-4BCD-97A6-F3447BF8C93A)
- [TextExpander touch SDK](drafts://open?uuid=A40970A6-8EF9-4388-AA28-C6AD4A1DF15E)
- [Friends Twitter List Export 12-15-2021](drafts://open?uuid=E18E5F6F-15A2-4A72-A823-BBFDA20751D1)
- [Index/Welcome](drafts://open?uuid=6BE78CAC-6238-4797-894D-59FDDB9E3CD7)
- [The Agony & The Ecstasy of the iPhone 12 Pro Max](drafts://open?uuid=07B641B2-6692-4FA9-9E7C-79D7EED5F803)
- [- [Shortcuts](https://www.icloud.com/shortcuts/a2ef3a327a0b4c32854cbe55e9a63080)](<drafts://open?uuid=6203D523-0B68-443B-BADE-F4A5D441FE86>)
- [contiguity](drafts://open?uuid=CAADDDDA-E504-4F17-9E72-09BEAD2905A3)
- [zachary7829‘s Blog](drafts://open?uuid=0AB593FC-8486-4FCE-AA8A-2F27438894A9)
- [David Blue’s Siri Shortcuts Repository](drafts://open?uuid=7998F086-4951-4DAB-995F-FE698FACFF78)
- [Tot Jar Shortcut](drafts://open?uuid=7A7DCF8B-BAE7-40B4-96E8-311BFC769722)
- [Hot Tot Dot Swap Shortcut](drafts://open?uuid=1E11D78E-134E-4583-A6E1-414855E66312)
- [Tot Markdown Backup](drafts://open?uuid=F145474F-4CD1-4053-BA14-5CFF16857362)
- [Dot Info Shortcut](drafts://open?uuid=5C07CFF1-1F28-4AA0-9704-FF52939536D3)
- [Batch Shortcuts Signer (iOS) Shortcut](drafts://open?uuid=03AEAC14-EB74-44F0-8010-9844B15E5DE3)
- [ThreadReaderApp Shortcut](drafts://open?uuid=FB1DCEEA-070E-4BD3-A1DD-113173F2B61D)
- [Producing a Reusable Signature on Apple Platforms with Siri Shortcuts](drafts://open?uuid=E11A8D6B-4CF4-4FC0-AE60-77C984DCB2AD)
- [Shortcuts Repo Index](drafts://open?uuid=031F557C-E046-48B3-832B-C91CAD466701)
- [David Blue on Tilde.Town](drafts://open?uuid=53D80A72-BE50-48E3-A25C-0799764CE4F2)
- [hacknorris Shortcuts](drafts://open?uuid=ABB5765F-EE2F-44D9-A384-9FBDB7277FCA)
- [unique\_fucks Shortcut](drafts://open?uuid=267A745A-49FC-4A7E-BD6C-5A26F64E14F7)
- [Howdy.txt](drafts://open?uuid=9C248F97-F061-44D3-A3A0-EE991C553123)
- [Tot](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0) [⇨](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0) [Apple Notes Shortcut](drafts://open?uuid=10EFCF5F-AAD4-4A4B-830A-9C02BDC766D0)
- [Run Shell Command Shortcut (Drafts)](drafts://open?uuid=EE9510FC-2EFB-409D-928F-A2E5C777B4C2)
- [Mac Menu Bar Shortcuts](drafts://open?uuid=68489605-5A2E-4250-8B8C-EC651CAEA449)
- [Automation April](drafts://open?uuid=114E449F-7518-4158-87EB-C817F633FBF2)
- [shortcutsshare.json](drafts://open?uuid=F8125570-2B56-4B08-A4B8-0428D25755F6)
- [WTF Drafts Index](drafts://open?uuid=8C4596E3-0397-468C-A579-CF56CB7CFE06)
- [7](drafts://open?uuid=5D709EBE-3738-4436-B24D-3EF10914EC60)
- [6](drafts://open?uuid=80F40208-15F0-4B84-9711-707E70D7D8A5)
- [5](drafts://open?uuid=6FB07C7A-41B6-4E40-8942-D82707E514B2)
- [4](drafts://open?uuid=3F5439FD-8587-4F98-A6ED-598810B393D5)
- [3](drafts://open?uuid=2734FEC9-9F48-431C-9EAA-0C9D79A29D03)
- [2](drafts://open?uuid=C4E91F15-95BC-4858-8922-86FE9F8FD303)
- [1](drafts://open?uuid=57AD10ED-5A99-438A-88E7-68C5F41D1564)
- [Notes-Drafts vs. Obsidian](drafts://open?uuid=C300D6BD-07A2-44E2-9231-54ADA771F952)
- [obsequious](drafts://open?uuid=825708FD-7DE8-4F5F-B087-B9664D26DCB8)
- [Notes-TildeTown on iPhone with Blink Shell](drafts://open?uuid=31122181-5565-4F1C-BEA3-C22DA140619A)
- [iOS System Sounds (WTF) Index](drafts://open?uuid=930C8B20-5C2F-4ECC-A311-4DFBF609AFD8)
- [WTF Vocabulary Index](drafts://open?uuid=49B2C528-FC99-4BBA-8C24-01E2525B1D0B)
- [NeoCities Homepage (WTF) 2.0](drafts://open?uuid=C2B1C951-518F-43FA-AEED-020CD5102A7B)
- [solipsism](drafts://open?uuid=CE388164-A884-40DF-BDC0-4F5E42D56286)
- [iOS System Sounds Raindrop Collection](drafts://open?uuid=72105F57-ADE2-44C0-A7F1-5C83F324E6CE)
- [tetchy](drafts://open?uuid=437B9FDD-7649-4512-973F-303CE3EB500A)
- [Kouki Izumi-Returning Nature to God](drafts://open?uuid=704E1607-1CE9-4CE2-BD04-E875A631AD32)
- [metaphor](drafts://open?uuid=D9D1D2B3-F0C2-47B6-A713-AAB487F6CC3C)
- [Shortcuts Man Page](drafts://open?uuid=1E60A236-6323-4EFF-9A4D-85C8B20907B9)
- [Shortcuts File Format Documentation](drafts://open?uuid=25521DC9-C40D-4E89-9FBD-0E8E0D157DCC)
- [Extract Archive Arbitrary Write bug](drafts://open?uuid=69A25067-94D3-48C3-8D46-6598A5C00380)
- [Testing Tot’s Shortcuts Actions Cross-Platform](drafts://open?uuid=452AC87B-E3DD-4BAF-8850-61FF6B51EC13)
- [Merriam-Webster API General Documentation](drafts://open?uuid=DD22C41E-6D86-4912-AC08-2CD3F83C0328)
- [List .md Drafts Open Links by Tag Shortcut](drafts://open?uuid=0E0613E7-9C1F-48DE-8AF8-ABA5E9619066)
- [Index Drafts Workspace Shortcut](drafts://open?uuid=BF7242C0-6E4E-4BD8-B01C-110A5F2BC26A)
- [Index by Tag with Drafts Open Links via Siri Shortcuts](drafts://open?uuid=225ED750-53C3-455C-9CAD-6CEBA0184ACC)
- [[[w:keys]] Index  - [[[[[[[[03832022-144011](tel:03832022-144011)](<tel:03832022-144011>)](<tel:03832022-144011>)](<tel:03832022-144011>)](<tel:03832022-144011>)](<tel:03832022-144011>)](<tel:03832022-144011>)](<tel:03832022-144011>)](<drafts://open?uuid=C9DEA2D3-DD7E-424D-A284-FA6513B5BE2F>)
- [[[w:correspondence]] Index  - [[[[[[[[03832022-142351](tel:03832022-142351)](<tel:03832022-142351>)](<tel:03832022-142351>)](<tel:03832022-142351>)](<tel:03832022-142351>)](<tel:03832022-142351>)](<tel:03832022-142351>)](<tel:03832022-142351>)](<drafts://open?uuid=D3E7A55F-91F0-41AF-88F6-59A233AA1E80>)
- [Vocabulary Index](drafts://open?uuid=F6B952AF-A7F8-4B2D-B310-8800099CF282)
- [[[w:Dev]] Index  - [[[[[[[[03832022-140156](tel:03832022-140156)](<tel:03832022-140156>)](<tel:03832022-140156>)](<tel:03832022-140156>)](<tel:03832022-140156>)](<tel:03832022-140156>)](<tel:03832022-140156>)](<tel:03832022-140156>)](<drafts://open?uuid=782F9576-D3B3-42CE-AAEB-00F2327EFAB1>)
- [placate](drafts://open?uuid=53543872-FBA2-4874-BE01-3D0243733507)
- [Shortcuts Actions are coming to Tot](drafts://open?uuid=DE4A4195-9D96-4CF0-922A-DB642CB6838F)
- [Drafts Autocompletes](drafts://open?uuid=D7135FD0-C2C5-4AB4-B0DD-DAAE7E70BA64)
- [Notes-Personal Twitter History](drafts://open?uuid=37B865E1-4372-4AF7-BB77-3979836130D4)
- [Curl Manual](drafts://open?uuid=EB2E0657-6F48-4759-8358-CCA486A7BD4D)
- [obloquy](drafts://open?uuid=AD7836E2-B6EE-420F-8FBA-4682D24C0830)
- [Tot Help, Merged](drafts://open?uuid=E4E1326D-7FF8-4E5D-9E91-8509D2789259)
- [WIFI:S:long1950;T:WPA;P:blue1950;;](drafts://open?uuid=4B746633-D89E-4201-996C-185D919DEA68)
- [bombastic](drafts://open?uuid=D1C34CD2-332C-4E19-869D-10D7191802E0)
- [recoup](drafts://open?uuid=63155184-4EE7-4DC5-80F4-000F206FB71F)
- [iconographic cannibalism](drafts://open?uuid=BAB523E3-840E-4456-9E03-C971C86EE214)
- [Thermal State Response | r/Shortcuts](drafts://open?uuid=C46229D3-83CF-4DE2-9230-346BB9D59E27)
- [macOS System Sounds Index](drafts://open?uuid=55B87318-A881-491D-8285-6D68456A144A)
- [Notes-Fresh Eyes on macOS](drafts://open?uuid=6C76EDBD-025D-40BA-9381-9AE95AD70F36)
- [JSON Speedtest Shortcut (macOS)](drafts://open?uuid=159CAD15-02B6-4429-AFA2-B06018E9A8CF)
- [Tot Version History](drafts://open?uuid=BADF99AB-3478-49C3-BF73-704C16C9E154)
- [Testing](drafts://open?uuid=C27A0B52-1B51-45D3-8641-0BD32AD72D4F)
- [gyaru moji - Google Search](https://www.google.com/search?q=gyaru%20moji)
- [Introducing Automation April: A Month-Long Community Event About Automation, Featuring Shortcuts, Interviews, Discord Workshops, and a Shortcut Contest](https://www.macstories.net/stories/introducing-automation-april/)
# David Blue’s WORLD FAMOUS Fast n’ Loose™ Siri Shortcuts for 𝚂𝙲𝚁𝚄𝙱𝚂
Updated `04022022-163836`
- [David Blue’s WORLD FAMOUS Fast n’ Loose™ Siri Shortcuts for](https://github.com/extratone/bilge/wiki/David-Blue%E2%80%99s-WORLD-FAMOUS-Fast-n%E2%80%99-Loose%E2%84%A2-Siri-Shortcuts-for-%F0%9D%9A%82%F0%9D%99%B2%F0%9D%9A%81%F0%9D%9A%84%F0%9D%99%B1%F0%9D%9A%82) [𝚂𝙲𝚁𝚄𝙱𝚂](https://github.com/extratone/bilge/wiki/David-Blue%E2%80%99s-WORLD-FAMOUS-Fast-n%E2%80%99-Loose%E2%84%A2-Siri-Shortcuts-for-%F0%9D%9A%82%F0%9D%99%B2%F0%9D%9A%81%F0%9D%9A%84%F0%9D%99%B1%F0%9D%9A%82) [· extratone/bilge Wiki](https://github.com/extratone/bilge/wiki/David-Blue%E2%80%99s-WORLD-FAMOUS-Fast-n%E2%80%99-Loose%E2%84%A2-Siri-Shortcuts-for-%F0%9D%9A%82%F0%9D%99%B2%F0%9D%9A%81%F0%9D%9A%84%F0%9D%99%B1%F0%9D%9A%82)
- [Shortlink](https://bit.ly/gitscrubs) - `[https://bit.ly/gitscrubs`](https://bit.ly/gitscrubs%60)
- [Pastery](https://www.pastery.net/eujzmz/#eujzmz)
- [WTF](https://davidblue.wtf/scrubs/)
- [WTF2](https://davidblue.wtf/drafts/68F049EC-59AF-4A49-885F-698BB9653400.html)
- [~](https://tilde.town/~extratone/scrubs/)
- [r/davidblue](https://reddit.com/r/davidblue/comments/sir6pm/david_blues_world_famous_fast_n_loose_siri/)
- [Siri Shortcut](https://routinehub.co/shortcut/10978/)
- [Siri Shortcut iCloud Direct Link](https://www.icloud.com/shortcuts/83c7c4ec1fcf4b359f520d5124899e9d)
- [Gist](https://gist.github.com/3f3748f2ee1cd0820ba099cb05809054)
- [Telegram](https://t.me/extratone/10927)

![OMNIMEMOJI](https://i.snap.as/p8U6kW1g.png)

**All** of the Siri Shortcuts I’ve created or modified over the years and thought worthy of sharing, prioritizing those which I have not shared on either my [RoutineHub](https://routinehub.co/user/blue) or [ShareShortcuts](https://shareshortcuts.com/u/blue/) profiles. You shouldn’t encounter share URLS from _any_ Shortcuts or File Shariing services, here - I thought a commitment to generating fresh `[icloud.com/shortcuts`](http://icloud.com/shortcuts%60)  URLs exclusively for this project would be in its best interest - I haven’t exactly been dilligently keeping track of them throughout the history of Siri Shortcuts as they’ve been exclusively shared this way…

## Most Utilitarian (In Theory…)

- [\*\*screen\*\*](https://www.icloud.com/shortcuts/6fa16145d1a649ceb878f141b8e595b7) - `[https://www.icloud.com/shortcuts/6fa16145d1a649ceb878f141b8e595b7`](https://www.icloud.com/shortcuts/6fa16145d1a649ceb878f141b8e595b7%60) 
- [undercovered](https://www.icloud.com/shortcuts/18eb1e77ab0b455f82da4c4c6e521368) (define)
- [style](https://www.icloud.com/shortcuts/3a2d212cc2614202be37b0beb8276e9b) (personal styleguide reference)
- [Text Replacement](https://www.icloud.com/shortcuts/e6d56b3fe8bc40639a60f166315f255b)
- [gps](https://www.icloud.com/shortcuts/c22c676ab4ee4b98b5a49b6112957b3c)
- [sl](https://www.icloud.com/shortcuts/7c3946b289a846cb9e63f0bdade93fa5) (The best Odesli quick share automation in history.)
- [Unpaywall](https://www.icloud.com/shortcuts/5ba5aa17593e4524843d69866800adb2)
- [TrippleTap Reminders](https://www.icloud.com/shortcuts/d17211cfac484c7f87ebd16b164ebce3)
- [fuckaudio](https://www.icloud.com/shortcuts/726eb2a7877446f8a8dc681947ee8f48)
- [Speedy Home Screen](https://www.icloud.com/shortcuts/4576d20841194a2e8c82569e7eecc9ec)
- [update](https://www.icloud.com/shortcuts/f9f53f99f16647f295e03e532d897965)
- [Markdown Capture (Comprehensive)](https://www.icloud.com/shortcuts/102f5ddb913f46239db48712d3e8115e)
- [Strip Image Metadata](https://www.icloud.com/shortcuts/e6121cf89ecf42ceba831251bbb4edef)
- [Jorts Device Report](https://www.icloud.com/shortcuts/a9fb15a922124770b8060fc19b8a9722)
- [Store Page URLS](https://www.icloud.com/shortcuts/9a60b0f4e03d440e8eefce7fefe51f8a) (Drafts)
- [Comprehensive Markdown Capture](https://www.icloud.com/shortcuts/94fe8ab9b16045068f23ec6c724cdec9)
- [temp](https://www.icloud.com/shortcuts/45030559a4864674b46634cecbc4bfc0) (Requires [Toolbox Pro](https://apps.apple.com/us/app/toolbox-pro-for-shortcuts/id1476205977) and [Data Jar](https://apps.apple.com/us/app/data-jar/id1453273600).)
- [Base64 Demo](https://www.icloud.com/shortcuts/6b9ed9e479a347438a1edbc95bee9b4c)
- [Base64 Photo](https://www.icloud.com/shortcuts/359624798fe046da83c01ec499aba67c)
- [cut](https://www.icloud.com/shortcuts/6418f65c0153408086cf15a5378c2e04)
- [Auto-Lock](https://www.icloud.com/shortcuts/28971c99d58145ebba88312fca59175c)
- [Add to LookUp Collection](https://www.icloud.com/shortcuts/09609fefe91340e0b4f07df5b332cafc)
- [DraftsImage](https://www.icloud.com/shortcuts/09d229c3b5064480a30ac784b7edd3ef)
- [DraftsImageSmall](https://www.icloud.com/shortcuts/fa3b63050cde48e4933bac6d9b1545df)
- [DATAFRUITS](https://www.icloud.com/shortcuts/bf26a791fe1a483fbf2ede5c47ae1d87)
- [O P E N](https://www.icloud.com/shortcuts/52e6c820f965488b91ca4b76c99434af)
- [DOWNLOAD](https://www.icloud.com/shortcuts/af5a886c9b044bf194dd52127a3939c3)
- [Hex Encode](https://www.icloud.com/shortcuts/2b2d578f53d94c139f112b381f1642cb)
- [NOW](https://www.icloud.com/shortcuts/c080b6245128491694875419160faec2)
- [unique\_fucks](https://www.icloud.com/shortcuts/0c654131c77240738a8a92073940f9d2)
- [Save to PodQueue](https://www.icloud.com/shortcuts/2665e5d29eed4096a122c4e8492e8bc9)

## Music/Audio Delights

- [\*\*Fondren & Main\*\*](https://www.icloud.com/shortcuts/fc846ff0e9094e088a7af4c5c11c3a23)
- [\*\*SHOW ME\*\*](https://www.icloud.com/shortcuts/029dea6d90f84223ac65be98fc803e7a) - `[https://bit.ly/showmesiri`](https://bit.ly/showmesiri%60)
- [RIDE THE STORM](https://www.icloud.com/shortcuts/50ea8b3e026f41668f9a2a88c702a4c4)
- [BREAKDALAW](https://www.icloud.com/shortcuts/0e175407850d4fcb83c64ea04c0c284f)
- [Frank Ocean - Street Fighter](https://www.icloud.com/shortcuts/ce89a2fe4f444e9c973436922640a33a)
- [HER](https://www.icloud.com/shortcuts/c6ab99cbc3d9430d87d4abbdf7714d16)
- [GOGO](https://www.icloud.com/shortcuts/80fa232621df4112bd1a92a5ace8e088)
- [FALTER](https://www.icloud.com/shortcuts/3f4efb0207ab4b0ca91606f160612273)
- [KEEPIN LIVE](https://www.icloud.com/shortcuts/f9a1161ae7da41ac8dd772010be5a049)
- [TKO](https://www.icloud.com/shortcuts/351c2593d3a2405691685fa6caf869aa)
- [FUTURELAND](https://www.icloud.com/shortcuts/3da59e509709437d90927ba65860fc3a) 
- [WITNESS](https://www.icloud.com/shortcuts/62afd3c029da456aa62d593de28145c4)
- [Meow](https://www.icloud.com/shortcuts/a32e7771f97c43feb5e228dc2c94a868)
- [Honk!](https://www.icloud.com/shortcuts/4dd2b2bf7110408b9fcde3f5811e5a02)
- [IKNOW](https://www.icloud.com/shortcuts/bed4693885b3425daa96deabf1d12e53)
- [SOS](https://www.icloud.com/shortcuts/bf3b733662144541ae4ab8fd317680a2)
- [AMEN](https://www.icloud.com/shortcuts/551499f76c774333956d678950971196)
- [Generation Rx](https://www.icloud.com/shortcuts/1d9d1bc51d3844b8a86510f224d9293a)
- [Angel](https://www.icloud.com/shortcuts/24d7c6dc0214495cb6303509575472d1)
- [Dinkus](https://www.icloud.com/shortcuts/5a86dd533fc34e1193a7fec67fdf0b63)
- [OSHA](https://www.icloud.com/shortcuts/c53b8e2f3d794da58445fa2806034f4b)

## Personal/Brand

- [The Psalms Ebook](https://www.icloud.com/shortcuts/eff33d5e122449deb190c70992022c97)
- [DavodTime](https://www.icloud.com/shortcuts/5a4ebd4ed057415b8915c535951c1059)
- [gurgle](https://www.icloud.com/shortcuts/21ffd45ece1d4443823a583ab722f4a9)

## Legacy/Drywall Media

- [trent](https://www.icloud.com/shortcuts/7a319d5b68b84fabb8e0c0bea441bb24)
- [birthday](https://www.icloud.com/shortcuts/4c2c7fd58aa84208bce08b69936aafbf)
- [WYGD](https://www.icloud.com/shortcuts/fd519a1a72c34a3d8559779826b23353)
- [Minecraft Flying Tutorial](https://www.icloud.com/shortcuts/a1c368c909c741a2864d0dd747115354)
- [Minecraft Flying Tutorial Ringtone](https://www.icloud.com/shortcuts/5f6570320d2c4097b815b56ba3df53f2)
- [homedepot](https://www.icloud.com/shortcuts/3409ec23198c4f34a82f08a8b680bd1e)

## Okay

- [NILLA](https://www.icloud.com/shortcuts/d0ed337c304f430993f2c8d8953d5e69)
- [BIG WORDLE](https://www.icloud.com/shortcuts/965dd56d81684881954be591303a962d)
- [COURDEROYED](https://www.icloud.com/shortcuts/75dc92926bed47af9a62faa83f2540d2)
- [Pappy](https://www.icloud.com/shortcuts/d797e3104318490aaa93c94361a3bbe0)
- [BIG COAT](https://www.icloud.com/shortcuts/0df2ab02738a4f46af70e0d16db79e56)

## Motion Pictures

- [I Hate This](https://www.icloud.com/shortcuts/de9514d4fb9343debc73331d813acecc)
- [come tweet](https://www.icloud.com/shortcuts/85b4f548c00644b8bc6728e2e130e1da)
- [NewGroove](https://www.icloud.com/shortcuts/069a37c98a224561a657f59660a006a1)
- [GeorgeStar](https://www.icloud.com/shortcuts/ce0caff9e2f747a189e89ce250ddb280)
- [OBAMNA](https://www.icloud.com/shortcuts/531cff2cf8234c239a241a60c9392eeb)
- [TEETH](https://www.icloud.com/shortcuts/76cd68a3e30a413da0dc86e9b1c40aa7)
- [Wii Shop Wednesday](https://www.icloud.com/shortcuts/37c4830c57c74a6a8980316dc14843dc)
- [HEAT WAVE](https://www.icloud.com/shortcuts/ba35c18707a44601b198a938a54291c6)

## Text Fuck

- [Random Characters](https://www.icloud.com/shortcuts/ed9216202df4481d9ae001b0531384c2) [⇨](https://www.icloud.com/shortcuts/ed9216202df4481d9ae001b0531384c2) [Clipboard](https://www.icloud.com/shortcuts/ed9216202df4481d9ae001b0531384c2)
- [Random Characters](https://www.icloud.com/shortcuts/af64b43604334d21ad5a6668471b828f) [⇨](https://www.icloud.com/shortcuts/af64b43604334d21ad5a6668471b828f) [Mastodon](https://www.icloud.com/shortcuts/af64b43604334d21ad5a6668471b828f)
- [Random Characters](https://www.icloud.com/shortcuts/399a857145e34d8b94b994fa3f9ca300) [⇨](https://www.icloud.com/shortcuts/399a857145e34d8b94b994fa3f9ca300) [Tumblr](https://www.icloud.com/shortcuts/399a857145e34d8b94b994fa3f9ca300)
- [Random Text](https://www.icloud.com/shortcuts/0873152dee3e4d32828cd28bcbc1be06) [⇨](https://www.icloud.com/shortcuts/0873152dee3e4d32828cd28bcbc1be06) [Twitter](https://www.icloud.com/shortcuts/0873152dee3e4d32828cd28bcbc1be06) ([Tweetbot](https://apps.apple.com/us/app/tweetbot-6-for-twitter/id1527500834))
- [Random Text](https://www.icloud.com/shortcuts/21ab008699ce44dabc9f9a249fc6f881) [⇨](https://www.icloud.com/shortcuts/21ab008699ce44dabc9f9a249fc6f881) [Twitter II](https://www.icloud.com/shortcuts/21ab008699ce44dabc9f9a249fc6f881)
- [Random Text](https://www.icloud.com/shortcuts/3bfc10474a254aec8a0f8f89da96d198) [⇨](https://www.icloud.com/shortcuts/3bfc10474a254aec8a0f8f89da96d198) [Clipboard](https://www.icloud.com/shortcuts/3bfc10474a254aec8a0f8f89da96d198)
- [Random Text](https://www.icloud.com/shortcuts/f550febfa39b465b88217e1717f37548) [⇨](https://www.icloud.com/shortcuts/f550febfa39b465b88217e1717f37548) [Drafts](https://www.icloud.com/shortcuts/f550febfa39b465b88217e1717f37548)
- [Random Text](https://www.icloud.com/shortcuts/0e517d1438b44d3d980c8afb9891a724) [⇨](https://www.icloud.com/shortcuts/0e517d1438b44d3d980c8afb9891a724) [Mastodon](https://www.icloud.com/shortcuts/0e517d1438b44d3d980c8afb9891a724)
- [Random Text](https://www.icloud.com/shortcuts/3c38ca0a7ec9413f9c9a6f6328fb1b09) [⇨](https://www.icloud.com/shortcuts/3c38ca0a7ec9413f9c9a6f6328fb1b09) [Tumblr](https://www.icloud.com/shortcuts/3c38ca0a7ec9413f9c9a6f6328fb1b09)
- [Random Text](https://www.icloud.com/shortcuts/07668aacd5ce4e59b76dd54ffc255209) [⇨](https://www.icloud.com/shortcuts/07668aacd5ce4e59b76dd54ffc255209) [WordPress](https://www.icloud.com/shortcuts/07668aacd5ce4e59b76dd54ffc255209)
- [Fuck](https://www.icloud.com/shortcuts/edcc737ed7304a17bb3b4d8897aae29c)
- [Tweet Symbol Images](https://www.icloud.com/shortcuts/a328611ebcab4f8ba271c0f89e3a7025)
- [Tweet Color Images](https://www.icloud.com/shortcuts/192008f53ad74860b1de1d7adccedb69)
- [Tweet Vocabulary Images](https://www.icloud.com/shortcuts/7417bf83a7a349099e1a50b1b091285b)
- [cowsay](https://www.icloud.com/shortcuts/4226e16277a849e29c9b94779e8a6f3e)

[Update Scrubs](shortcuts://run-shortcut?name=Update%20Scrubs)
- [Retrieve User's Posts – Write.as API Documentation](https://developers.write.as/docs/api/#retrieve-user-39-s-posts)

```html
<!DOCTYPE html>
<html>
   <head>
      <title>Consultation</title>
      <meta http-equiv = “refresh” content = “0; url = <<<<<<<<https://fantastical.app/davidblue/general-consultation>>>>>>>>” />
   </head>
   <body>
   </body>
</html>
```
**Dot: 7**
- Lines: 691
- Words: 6329
- Characters: 51264
- Modified: Apr 8, 2022 at 18:41

`ia download nhr_Carl_Cox --glob="\*.mp3"`
# Notes-Drafts vs. Obsidian
- [GitHub Issue](https://github.com/extratone/bilge/issues/301)
- [Drafts vs. Obsidian Stuff · Issue #22 · extratone/drafts](https://github.com/extratone/drafts/issues/22)
- [["Compiling and Exporting Tagged Notes in Drafts 5"]]
- [Compiling and Exporting Tagged Notes in Drafts 5](<https://club.macstories.net/posts/compiling-and-exporting-tagged-notes-in-drafts-5>) | _MacStories_
- [Drafts Directory: Export Review Draft](https://actions.getdrafts.com/a/1L7)
- [SoFar SimpleNote](http://simp.ly/publish/ffZXcw)

## General

- [ ] Might just be because my systems are limited in performance?
- [ ] The one thing Drafts cannot offer and probably never will: true cross-platform integration with Windows. That's fucking okay, though.
- [ ] It’s not that I’m against Electron apps - as a Windows user, I cannot afford to be discriminating whatsoever when it comes to new, functional software.
- [ ] [Native file integrations](https://docs.getdrafts.com/docs/actions/steps/services) with Twitter, Dropbox, Evernote, WordPress, Medium, Google Drive, Gmail, Google Tasks, Box, OneDrive, OneNote, Outlook, Microsoft To Do, Todoist, and any WebDAV-enabled file server, [as I’ve demonstrated](https://bilge.world/using-drafts-with-neocities).
- [ ] I fundamentally _trust_ Drafts more than any other notetaking/word living application I've ever used, and a good deal of that comes from what feels like an extremely direct line to Greg.
- [ ] Let me see you live switch back and forth between devices editing the same documents with any coherence in Obsidian, eh? Git is wonderful, but it was built to be used like we’re using it, and we need to consider that. Indeed, what if iCloud Drive sync is mostly an advantage? _Especially_ when it’s Native As Fuck.
- [ ] I just a moment ago rid my main repository of Obsidian for good and it was almost 1000 files.
- [ ] Still much much more out of the box because it is so in a different way - I keep discovering things Drafts will do for me instead of something else, but better, where I feel like Obsidian is all about finding more stuff to do.
- [ ] Using draft_open_links and Tot, I have created my own dashboard outside of Drafts, which works especially beautifully on macOS.
- [ ] Instant synonyms with `^⌥S` calling Stephen Millard’s `TAD-Replace With Synonym` action. (As part of [ThoughtAsylum-Writing Action Group](https://actions.getdrafts.com/g/1cu))
- [ ] In essence, I’m just worried.
- [x] Backwards compatibility! - I was able to sync all ~1600 drafts in my library + themes + actions in around 5 minutes on an eight-year-old iPad Air.
- [ ] Launching webpages from my Test note because of TextExpander integration.
- [ ] How easy it was to [repair the TAD issue](<https://forums.getdrafts.com/t/script-error-syntaxerror-json-parse-error-unexpected-identifier-drafts-line-number-53-column-35/12187>).
- [ ] Posting to WordPress/Medium with a single step.
- [ ] [Taio](https://apps.apple.com/us/app/taio-markdown-text-actions/id1527036273) is perhaps even more redundant in regards to Drafts, but establishing its own identity.
- [ ] [Actions for Obsidian in the Action Directory]([[[[[https://actions.getdrafts.com/search?utf8=✓&q=obsidian](https://actions.getdrafts.com/search?utf8=%E2%9C%93&q=obsidian)](<https://actions.getdrafts.com/search?utf8=%E2%9C%93&q=obsidian>)](<https://actions.getdrafts.com/search?utf8=%E2%9C%93&q=obsidian>)](<https://actions.getdrafts.com/search?utf8=%E2%9C%93&q=obsidian>)](<https://actions.getdrafts.com/search?utf8=%E2%9C%93&q=obsidian>)).
- [ ] Drafts’ absurdly high rating in the App Store.
- [ ] [Live preview update - Obsidian Help](https://help.obsidian.md/Live+preview+update)
- [ ] [Folding Text / Collapsable Headers - Feature Requests - Drafts Community](https://forums.getdrafts.com/t/folding-text-collapsable-headers/1034) - it looks like I may be one of few Drafts users who is _uninterested_ in folding headers. In the several implementations I’ve tried them over the years, they tend to just confuse me / get me lost. 

## Misc

- [ ] [Obsidian Attempt · Issue #219 · extratone/bilge](https://github.com/extratone/bilge/issues/219)
- [ ] Wikilinks are actually really dangerous.
- [ ] Direct integration with Agiltortoise’s [Terminology](https://apps.apple.com/us/app/terminology-dictionary/id687798859) remains unique, compelling, and far superior, ridiculously, to the subscription fee-requiring LookUp.
- [ ] [RoutineHub - List .md Drafts Open Links by Tag](https://routinehub.co/shortcut/11085/)
- [ ] [Index by Tag with Drafts Open Links via Siri Shortcuts - Tips & Tricks - Drafts Community](https://forums.getdrafts.com/t/index-by-tag-with-drafts-open-links-via-siri-shortcuts/12296/2)
- [ ] [Drafts Directory: On My Mind](https://directory.getdrafts.com/g/1yM)
- [ ] [On My Mind Action Group by FlohGro](drafts://open?uuid=C90B70B3-A594-4736-86B2-E4F1924D3096)
- [ ] [Would you consider offering a GitBook theme on Obsidian?  · Issue #38 · h16nning/typora-gitbook-theme](https://github.com/h16nning/typora-gitbook-theme/issues/38) (NO.)

## Social

- [another advantage of Drafts being Native as Fuck… this action runs entirely in the background.](https://twitter.com/NeoYokel/status/1490748762779791360)
- [Switching between drafts with numeric keyboard shortcuts](https://twitter.com/NeoYokel/status/1492567120336535552)
- [Wide Boy](https://twitter.com/NeoYokel/status/1493380856223838215)
- [Greg’s Drafts 31 Tweet](https://twitter.com/agiletortoise/status/1495240934556057605)
- [Backwards compatibility Tweet](https://twitter.com/NeoYokel/status/1498412135268982784)
- [Greg’s 14 years ago today Tweet](https://twitter.com/agiletortoise/status/1500939960207253509)
- [I found another Taio user](https://mastodon.xyz/@CutThroatNeko/107925765618277225)
- [Federico’s holiday project](https://twitter.com/viticci/status/1481687474753658890)

## Images

- [ ] `![SpaceTakenbyDrafts](https://user-images.githubusercontent.com/43663476/153721368-59fbb89f-46ff-46da-8caf-8d24047e3589.png)`
- [ ] `![BookmarksinDrafts](https://user-images.githubusercontent.com/43663476/153778986-a9a33a80-12a3-4042-8941-2e2d5e427703.png)`
- [x] `![Black&YellowTheme](<https://user-images.githubusercontent.com/43663476/153780995-5b7ac09d-4c21-4e61-b1ba-163ce106fb01.png>)`
- [ ] `![FullSerifMode](https://user-images.githubusercontent.com/43663476/153918963-22bee968-92ac-441c-8535-e90c4658840c.png)`
- [ ] `![HalfSerifMode](https://user-images.githubusercontent.com/43663476/153919115-6d34655e-ed9e-4d28-b3e4-55b136d8ba37.png)`
- [ ] `![ArrangeMode](https://user-images.githubusercontent.com/43663476/153938891-ff5312ba-31d4-415f-88e4-e3e14925575f.png)`
- [ ] ![SynonymsWithaKeyboardShortcut](https://user-images.githubusercontent.com/43663476/153939391-c92f75c1-1313-4ead-96fa-c6ef0927a59d.png)
- [ ] `![Autocomplete](https://user-images.githubusercontent.com/43663476/153939600-e26cd8eb-b78f-4d67-a5fb-993b508830f9.png)`
- [ ] `![TerminologyIntegration](https://user-images.githubusercontent.com/43663476/153948463-31435ed0-76e7-4b33-be9f-3089248e34f4.png)`
- [ ] ![ReminderIntegrations](https://user-images.githubusercontent.com/43663476/153950328-e0e71931-d407-469f-a882-b9566f37399c.png)
- [x] `![ControllingBearfromDrafts](<https://user-images.githubusercontent.com/43663476/153966641-fad3a074-4938-4cef-b1c8-ed900cd8599c.png>)`
- [ ] `![TriggeringBearfromDrafts](https://user-images.githubusercontent.com/43663476/153966718-f51329cd-5928-4127-9958-42d972882521.png)`
- [ ] `![ArrangeMode](https://user-images.githubusercontent.com/43663476/154339203-1c30713e-cb53-40c6-8e70-7f75c1dfdee2.png)`
- [ ] `![ArrangeModeLandscape](<https://user-images.githubusercontent.com/43663476/154815196-48e6535f-e740-4c6c-> b40b-7db3ab190d27.png)`
- [ ] `![ExportPsalmsActionsLandscape](https://user-images.githubusercontent.com/43663476/154817269-0713a89d-d1a0-46a1-b2ed-57a84abddff8.png)`
- [x] `![TipCalculatorLandscape](<https://user-images.githubusercontent.com/43663476/154821804-3d6d90b4-76d4-4082-a4dc-7dde5631c296.png>)`
- [ ] `![Sending Drafts from iMessage](https://user-images.githubusercontent.com/43663476/154845607-1dc5e1c5-759a-4030-84ab-d609491f7acb.png)`
- [ ] `![Long Press to Change Editor Options](https://user-images.githubusercontent.com/43663476/154864287-4f2d21d5-d332-4e75-8c81-9fb74cde1f70.png)`
- [ ] `![Find & Replace with Regular Expressions](https://user-images.githubusercontent.com/43663476/154865771-47d8565b-424f-477e-b664-770a1b0d2fb1.png)`
- [ ] [[The LightRedDot Theme by jamiejenkins]]
- [ ] `![Bellelle Dark Theme](<https://user-images.githubusercontent.com/43663476/155053127-40026c08-e839-4ec3-996e-3ed0660a8a2d.png>)`
- [ ] `![Grey and Orange Theme](https://user-images.githubusercontent.com/43663476/155100185-b8270067-abb4-4735-825d-c70cd2ee4d53.png)`
- [ ] `![Grey and Orange Theme](https://user-images.githubusercontent.com/43663476/155100214-407d1546-3ce0-46ef-b2c7-7702fc18f0f4.png)`
- [x] `![Terminology Draft](<https://user-images.githubusercontent.com/43663476/155904725-8489c896-adfc-4f43-8901-eb897ab998e4.png>)`
- [x] `![aforelinked in Terminology](<https://user-images.githubusercontent.com/43663476/155905726-ffa126d6-ccb5-497f-a80c-51ba457b1330.png>)`
- [x] `![LookUp Failure](<https://user-images.githubusercontent.com/43663476/155922296-4c00c372-8b79-4dfd-b421-5d718845a782.png>)`
- [ ] `![Written Folder](https://user-images.githubusercontent.com/43663476/155971770-907a90aa-c955-4928-bbad-6cd1a61216ea.png)`
- [ ] `![Quick Navigation](https://user-images.githubusercontent.com/43663476/156029543-89c96f8f-0a23-4434-8514-a6e8349bb565.png)`
- [x] `![Obsidian Publish Example](<https://user-images.githubusercontent.com/43663476/156826733-4e596f5f-2f90-4e88-977e-fb6dc8f9c218.png>)`
- [x] `![Versioning in Drafts](<https://user-images.githubusercontent.com/43663476/156828832-b8c4554f-a7b9-4ff3-997b-329e6a227e57.png>)`
- [ ] `![Committing Drafts in Working Copy](https://user-images.githubusercontent.com/43663476/156829737-65cf15d3-9118-4f06-8796-5097c474e7a2.png)`
- [ ] `![Notes Organization - Drafts vs Bear](https://user-images.githubusercontent.com/43663476/158259554-5191b921-8064-4ac6-8720-f9c5fa3c67b8.png)`
- [ ] `![A Complete Mess](https://user-images.githubusercontent.com/43663476/160045048-23b34db5-0aa3-430b-8453-c52258646115.png)`

## Videos

- [Everything You Need to Know about Obsidian Sync - YouTube](https://youtube.com/watch?v=XC40RgvH1D4)
- [Terminology Integration](https://user-images.githubusercontent.com/43663476/154864296-c6cc7b2d-ce04-444e-9e27-0f0ddef252d7.MOV)
<video controls>
  <source src="<<<<<https://user-images.githubusercontent.com/43663476/154864296-c6cc7b2d-ce04-444e-9e27-0f0ddef252d7.MOV>>>>>">
</video>
- [Arrange Mode Demo](https://imgur.com/gallery/6NdIiVJ)
<video controls>
  <source src="<<<<<https://user-images.githubusercontent.com/43663476/155023319-c031aa8c-706e-442c-a5a6-59d0adf95e03.MOV>>>>>">
</video>

## References

- [ ] [Bookmarks - Drafts User Guide](https://docs.getdrafts.com/docs/settings/bookmarks)
- [ ] [Services - Drafts User Guide](https://docs.getdrafts.com/docs/actions/steps/services)
- [ ] [iTextEditors - iPhone and iPad text/code editors and writing tools compared](https://brettterpstra.com/ios-text-editors/#obsidian)
- [ ] [My Markdown Writing and Collaboration Workflow, Powered by Working Copy 3.6, iCloud Drive, and GitHub - MacStories](https://www.macstories.net/ios/my-markdown-writing-and-collaboration-workflow-powered-by-working-copy-3-6-icloud-drive-and-github/)
- [ ] [Working Copy Integrates with the Files App and Drag and Drop - MacStories](https://www.macstories.net/reviews/working-copy-integrates-with-the-files-app-and-drag-and-drop/)
- [ ] [iPad Diaries: Working with Drag and Drop – Bear and Gladys - MacStories](https://www.macstories.net/ios/ipad-diaries-working-with-drag-and-drop-bear-and-gladys/)
- [ ] [Interview: Greg Pierce](https://club.macstories.net/posts/interview-greg-pierce-1)
> There will always be advantages to being fully native on the platform.
- [ ] [Drafts 5 Review | Rosemary Orchard](https://rosemaryorchard.com/blog/my-drafts-5-review/)
- [ ] [Drafts 5: The MacStories Review - MacStories](https://www.macstories.net/reviews/drafts-5-the-macstories-review/)
- [ ] [Drafts 26: The Customization Update](https://nahumck.me/drafts-26-the-customization-update/)
- [ ] [Drafts Web Capture](https://docs.getdrafts.com/docs/extensions/web-capture)
- [ ] [App Modularity - AppStories+](https://pca.st/episode/75b22c91-745b-41f3-83fb-7f6d2f3d335b)
- [ ] [Drafts 29: Autocomplete](https://nahumck.me/drafts-29-autocomplete/)
- [ ] [Delete Your Notes](https://club.macstories.net/posts/delete-your-notes)
- [ ] [Everything but the Text Editor](https://club.macstories.net/episodes/203)
- [ ] [Searching for the perfect iOS Markdown writing tool](https://sixcolors.com/post/2021/03/searching-for-the-perfect-ios-markdown-writing-tool/)
- [ ] The whole [Attributions list](https://docs.getdrafts.com/docs/misc/attributions) for Drafts is just _seven items in length_.
- [ ] [The Fall of Roam](https://every.to/superorganizers/the-fall-of-roam)
- [ ] [Drafts 5.4: Siri Shortcuts, WordPress, and More](https://www.macstories.net/reviews/drafts-5-4-siri-shortcuts-wordpress-and-more/) | _MacStories_
- [ ] [How I Make Drafts Work for Me — HeyDingus](https://heydingus.net/blog/2021/7/how-i-make-drafts-work-for-me)
- [x] [Setup: Drafts on iOS with Working Copy - Tips & Tricks - Drafts Community](<https://forums.getdrafts.com/t/setup-drafts-on-ios-with-working-copy/9197>)
- [x] [Version History](<https://docs.getdrafts.com/docs/drafts/versionhistory>)
- [ ] [Drafts 2.0: New Fonts, New Look, A Brand New Kind of Sync, And Now On iPad](https://www.macstories.net/reviews/drafts-2-0-new-fonts-new-look-a-brand-new-kind-of-sync-and-now-on-ipad/)
- [ ] [Drafts 2.0  New Fonts, New Look, A Brand New Kind of Sync, And Now On iPad](drafts://open?uuid=C3670318-F206-4414-A6D5-17E4898CB897)
- [ ] [Drafts Review](drafts://open?uuid=391F1351-B2B2-4E9C-B5C7-79C0F3DA4B18)
- [ ] [Drafts, Shortcuts, and Working Copy, The Movie](https://scottwillsey.com/blog/ios/draftsthemovie/)
- [ ] [Choosing Obsidian over Roam????](https://youtu.be/AWUk8-6yG2g)
- [ ] - [Referencing an image from photos app - iOS - Actions - Help & Questions - Drafts Community](https://forums.getdrafts.com/t/referencing-an-image-from-photos-app-ios/11325/5)

> In Obsidian, the file in photos is copied into the Obsidian vault when you select it. Hence it can use a relative reference to the file at that point.
> Drafts could easily insert the text content for the image reference, and send that over to Obsidian. The tricky bit however is referencing an image held in the photos app.
> As a text-based tool, Drafts does have the capability to do some work with files, but primarily text files, and nothing for interacting with files held within the Apple Photos application - they are not available through the files app.
> In a similar Shortcuts-based approach to the one used by @tf2, you could replicate the Obsidian process (not using external hosting), and you can integrate that with an action in Drafts that adds the image reference. But Drafts alone is not currently able to interact directly with Apple Photos.

- [How to process notes in Obsidian // Readwise Official Obsidian plugin](https://youtube.com/watch?v=Rw1L5sxlnuU&feature=share)
- [My Obsidian Setup, Part 8: Simplifying Thought Capture with 'On My Mind'](https://club.macstories.net/posts/my-obsidian-setup-part-8-simplifying-thought-capture-with-on-my-mind)

## Obsidian Documentation

- [Using obsidian URI - Obsidian Help](https://help.obsidian.md/Advanced+topics/Using+obsidian+URI)
- [Obsidian - Obsidian Help](https://help.obsidian.md/Obsidian/Obsidian#How+we're+different) - The about page quotes fucking John Locke lmfao.
- [Obsidian - Obsidian Help](drafts://open?uuid=1657C00E-CCFB-414C-8BE2-E31B719127CC)
- [Obsidian Roadmap | Trello](https://trello.com/b/Psqfqp7I/obsidian-roadmap)
- [My Obsidian Setup, Part 9-Saving Articles and Music Albums for Later with Shortcuts and Dataview](drafts://open?uuid=606E4131-B901-481C-AEDA-2EEDA1F8E1C4)
- [My Favorite Obsidian Plugins for Automating the Management of Notes](drafts://open?uuid=047F0715-7D39-4178-AAA4-4627111888BA)

```
<obsidian://open?vault=Testing&file=Colophon>
<obsidian://advanced-uri?vault=Testing&filepath=Colophon.md&heading=Colophon>
```

[The Automation Myth | Clinton Williamson](https://thebaffler.com/latest/the-automation-myth-williamson)

Tim Nahumck’s “**[A Decade of Drafts](https://www.nahumck.me/a-decade-of-drafts/)**” is a very special way to start off the group, I think. I thought I could at least contribute a Siri Speech Synthesis read aloud.
